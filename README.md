# Git2P lib

This workspace attemps to write git's functionality as a reusable library instead of an executable,
the goal is to provide an useful library for building both git clients and servers. But is primary
focused on servers.

This isn't a lib for interact with `Git2P` network, if you are looking for start interacting with 
the network maybe you want to see `Git2PClient`

# Status 

This library is not usable yet. It is a convenience implementation for the needs that arise from the development of git2p and its surrounding ecosystem.  Therefore the development of this library depends directly on the progress of git2p.

There is also a lot of broken stuff here, for example the examples, the only reason to make it public at this point is that I am sick of having to deal with ssh keys and dependencies in private git repositories, you know I turn on the computer my mind lights up, I do `cargo run` and it fails

# Development

# Usage

# Examples

```bash
cargo run -p $example

```

- cat-file
this example iterates over a git's odb of repository given its project-path and prints each object.
```bash
cargo run -p cat-file

```

# Non-goals

- Replace or compete with git.

This library can be used to build git clients because the primitives are basically the same on both 
sides (client/server), but the main purpose of this library is to be used for build git servers, we 
don't compete with git (it doesn't make sense, git is one of the best inventions in the computers's 
history), we compete with abusive and coercive code hosting services like github.

# Chat

- Community

[Matrix](https://matrix.to/#/#git2p:matrix.org)

- Personal

[Matrix](https://matrix.to/#/@0al3x:matrix.org)
[Telegram](https://t.me/al3x00)
[Discord](https://discordapp.com/users/728754894328037478)

# License

see [LICENSE](./LICENSE)

# Donations

I'm not a bad engineer but my work advance very slowly because I'm living at the border, if you want
to support to me you can do it by:

- patreon: https://www.patreon.com/04l3x
- btc: bc1qqsaacwvthz6kn3jpgt03r0hunvtrv4ll7ldcm0k

# Random and fails playgrouns

https://play.rust-lang.org/?version=stable&mode=debug&edition=2021&gist=c31b093950133cf70b696eef66d020e1


