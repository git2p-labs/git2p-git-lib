/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use git2p_git_lib::{
	git_blob::blob::Blob,
	git_commit::commit::Commit,
	git_object::object::Object,
	git_odb::odb::{GitOdb, OdbObject},
	git_repository::repository::{Odb, Open, Repository},
	git_tree::tree::Tree,
};
use std::path::PathBuf;

fn main() {
	let path = PathBuf::from("../../");

	let odb = Repository::open(&path).unwrap().odb().unwrap();

	// Odb "implements" `IntoIterator` (not really only have an asociate type what is the odb
	// iterator per se).
	// Here we obtain the obd iterator and iterate over each odb-object
	odb.iter().iter().for_each(|o| {
		let obj = odb.read(o).unwrap();

		show_object(obj);

		// velocity of each iteration
		// higher is slowly
		// less is quickly
		let delay = 10;

		std::thread::sleep(std::time::Duration::new(0, 1000000000 / delay));

		clear();
	});
}

fn show_object(obj: OdbObject) {
	match obj {
		OdbObject::Blob(b) => show_blob(b),
		OdbObject::Tree(t) => show_tree(t),
		OdbObject::Commit(c) => show_commit(c),
	}
}

fn show_blob(blob: Blob) {
	let oid = blob.oid().unwrap();

	let content = String::from_utf8(blob.content().to_vec());

	// non-uft8 strings are not handled yet
	if content.is_ok() {
		println!("{}", content.unwrap())
	} else {
		println!("Bob with id: {:x}, contains non utf8 characters", oid);
	}
}

fn show_tree(tree: Tree) {
	println!("{:#?}", tree.entries());
}

fn show_commit(commit: Commit) {
	println!("{:#?}", commit.message());
}

fn clear() {
	clearscreen::clear().unwrap();
}
