/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#[cfg(feature = "author")]
pub use git2p_git_author as git_author;

#[cfg(feature = "blob")]
pub use git2p_git_blob as git_blob;

#[cfg(feature = "commit")]
pub use git2p_git_commit as git_commit;

#[cfg(feature = "config")]
pub use git2p_git_config as git_config;

#[cfg(feature = "index")]
pub use git2p_git_index as git_index;

#[cfg(feature = "object")]
pub use git2p_git_object as git_object;

#[cfg(feature = "odb")]
pub use git2p_git_odb as git_odb;

#[cfg(feature = "oid")]
pub use git2p_git_oid as git_oid;

#[cfg(not(target_family = "wasm"))]
#[cfg(feature = "pack")]
pub use git2p_git_pack as git_pack;

#[cfg(feature = "path")]
pub use git2p_git_path as git_path;

#[cfg(feature = "pktline")]
pub use git2p_git_pktline as git_pktline;

#[cfg(feature = "references")]
pub use git2p_git_references as git_references;

#[cfg(feature = "repository")]
pub use git2p_git_repository as git_repository;

#[cfg(feature = "serializers")]
pub use git2p_git_serializers as git_serializers;

#[cfg(feature = "time")]
pub use git2p_git_time as git_time;

#[cfg(feature = "tree")]
pub use git2p_git_tree as git_tree;
