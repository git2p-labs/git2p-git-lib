/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

pub const NULL: u8 = 0x00;
pub const SPACE: u8 = 0x20;
pub const _PLUS: u8 = 0x2b;
pub const _MINUS: u8 = 0x2d;
pub const LF: u8 = 0x0a;
pub const LT: u8 = 0x3c;
pub const _EQ: u8 = 0x3d;
pub const GT: u8 = 0x3e;
