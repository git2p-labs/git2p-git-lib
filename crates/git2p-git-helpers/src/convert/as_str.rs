/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::convert::{error::Error, Result};

pub trait AsStr {
	fn as_str(&self) -> Result<&str>;
}

impl AsStr for &[u8] {
	fn as_str(&self) -> Result<&str> {
		if let Ok(s) = std::str::from_utf8(self) {
			Ok(s)
		} else {
			Err(Error::BadStringCharset)
		}
	}
}

impl AsStr for Vec<u8> {
	fn as_str(&self) -> Result<&str> {
		if let Ok(s) = std::str::from_utf8(self) {
			Ok(s)
		} else {
			Err(Error::BadStringCharset)
		}
	}
}
