/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::convert::{as_str::AsStr, error::Error, Result};
use std::str::FromStr;

pub trait StrArrayTo {
	fn str_arr_to<T>(&self) -> Result<T>
	where
		T: FromStr;
}

impl StrArrayTo for &[u8] {
	fn str_arr_to<T>(&self) -> Result<T>
	where
		T: FromStr,
	{
		if let Ok(t) = T::from_str(self.as_str()?) {
			Ok(t)
		} else {
			Err(Error::BadStringInput)
		}
	}
}
