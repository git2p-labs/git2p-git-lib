/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::convert::{error::Error, Result};

pub trait SplitBy<T>
where
	T: Eq + PartialEq,
{
	fn split_by(&self, elem: &T) -> Result<(&[T], &[T])>;
}

impl<T> SplitBy<T> for &[T]
where
	T: Eq + PartialEq,
{
	fn split_by(&self, elem: &T) -> Result<(&[T], &[T])> {
		let mid = self.iter().position(|e| e.eq(elem));

		if mid.is_none() {
			return Err(Error::ElementDoesNotExist);
		};

		let (l, r) = self.split_at(mid.unwrap());

		Ok((l, &r[1..]))
	}
}
