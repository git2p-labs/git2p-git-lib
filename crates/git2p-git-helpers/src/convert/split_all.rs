/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::convert::Result;

pub trait SplitAll<T>
where
	T: Eq + PartialEq,
{
	fn split_all(&self, elem: &T) -> Result<Vec<&[T]>>;
}

impl<T> SplitAll<T> for &[T]
where
	T: Eq + PartialEq,
{
	fn split_all(&self, elem: &T) -> Result<Vec<&[T]>> {
		Ok(self.split(|e| e.eq(elem)).collect())
	}
}
