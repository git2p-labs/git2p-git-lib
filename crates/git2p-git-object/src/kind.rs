/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use std::fmt::Display;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum ObjectKind {
	Blob,
	Commit,
	Tree,
	Tag,
	Any,
}

impl ObjectKind {
	pub fn is_blob(&self) -> bool {
		matches!(self, Self::Blob)
	}

	pub fn is_commit(&self) -> bool {
		matches!(self, Self::Commit)
	}

	pub fn is_tree(&self) -> bool {
		matches!(self, Self::Tree)
	}

	pub fn is_tag(&self) -> bool {
		matches!(self, Self::Tag)
	}

	pub fn is_any(&self) -> bool {
		matches!(self, Self::Any)
	}
}

impl Display for ObjectKind {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		let kind = format!("{self:?}");
		write!(f, "{}", kind.to_lowercase())
	}
}

impl From<&[u8]> for ObjectKind {
	fn from(value: &[u8]) -> Self {
		match value {
			b"blob" => Self::Blob,
			b"commit" => Self::Commit,
			b"tree" => Self::Tree,
			b"tag" => Self::Tag,
			_ => Self::Any,
		}
	}
}

impl From<&ObjectKind> for Vec<u8> {
	fn from(value: &ObjectKind) -> Self {
		value.to_string().as_bytes().to_vec()
	}
}
