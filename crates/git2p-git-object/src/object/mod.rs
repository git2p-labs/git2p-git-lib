/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{kind::ObjectKind, result::Result};
use git2p_git_oid::oid::Oid;
use git2p_git_serializers::{de::TryDeserialize, ser::Serialize};
use sha1::{Digest, Sha1};

mod header;
pub use header::Header;

pub trait Object<'obj, E>: Serialize<'obj, Vec<u8>> + TryDeserialize<'obj, &'obj [u8], E> {
	fn oid(&self) -> Result<Oid> {
		let mut hasher = Sha1::new();
		hasher.update(&self.serialized());
		let hash = hasher.finalize();

		Ok(Oid::try_from(hash.as_slice())?)
	}

	fn header(&self) -> header::Header;

	fn serialized(&self) -> Vec<u8>;

	fn kind(&self) -> ObjectKind {
		self.header().kind()
	}

	fn from_raw(raw: &[u8]) -> std::result::Result<Self, E>;
}
