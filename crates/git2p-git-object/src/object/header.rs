/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{error::Error, kind::ObjectKind, result::Result};
use git2p_git_helpers::{
	ascii::{NULL, SPACE},
	convert::SplitBy,
};
use git2p_git_serializers::{de::TryDeserialize, ser::Serialize};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Header {
	kind: ObjectKind,
	size: usize,
}

impl Header {
	pub fn new(kind: ObjectKind, size: usize) -> Self {
		Self { kind, size }
	}

	pub fn try_from_raw(raw: &[u8]) -> Result<Self> {
		let (header, _) = raw.split_by(&NULL)?;
		Self::try_deserialize(&header)
	}

	pub fn kind(&self) -> ObjectKind {
		self.kind
	}

	pub fn size(&self) -> usize {
		self.size
	}

	pub fn of_raw(raw: &[u8]) -> Result<Self> {
		let (header, _) = raw.split_by(&NULL)?;

		Self::try_deserialize(&header)
	}
}

git2p_git_serializers::serialize_impl!(Header, Vec<u8>, 'ser);

impl From<&Header> for Vec<u8> {
	fn from(value: &Header) -> Self {
		[
			value.kind.to_string().as_bytes(),
			&[SPACE],
			value.size.to_string().as_bytes(),
		]
		.concat()
	}
}

git2p_git_serializers::deserialize_impl!(Header, &'de [u8], Error, 'de);

impl TryFrom<&&[u8]> for Header {
	type Error = Error;

	fn try_from(value: &&[u8]) -> Result<Self> {
		let (kind, size) = value.split_by(&SPACE)?;

		let kind = ObjectKind::from(kind);

		let size = String::from_utf8(size.to_vec()).unwrap();

		let size = size.parse::<usize>().unwrap();

		Ok(Self { kind, size })
	}
}
