/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{error::Error, result::Result};
use git2p_git_helpers::{
	ascii::{NULL, SPACE},
	convert::{AsStr, SplitBy},
};
use git2p_git_oid::oid::Oid;
// use git2p_git_path::{FileName, Git2PPath, Parent};
use git2p_git_serializers::{de::TryDeserialize, ser::Serialize};
use serde::{Deserialize, Serialize as Ser};
use std::fmt::Display;

pub mod mode;
use mode::EntryMode;

#[derive(Clone, Debug, Eq, Ser, Deserialize)]
pub struct Entry {
	mode: EntryMode,
	oid: Oid,
	// path: Git2PPath,

	// for /my/repo/my/entry
	// /my/repo/my is the parent tree
	// entry is the name
	name: String,
}

impl Default for Entry {
	fn default() -> Self {
		Self {
			mode: EntryMode::Tree,
			oid: Oid::zero_id_sha1(),
			name: String::new(),
		}
	}
}

impl Display for Entry {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		let mode = format!("{:?}", self.mode);

		write!(
			f,
			"{:06} {} {}\t{}",
			self.mode as i32,
			mode.to_lowercase(),
			self.oid,
			self.name()
		)
	}
}

//NOTE: old implementation //self.path.cmp(&other.path)
impl Ord for Entry {
	fn cmp(&self, other: &Self) -> std::cmp::Ordering {
		let mut lhs = self.name.clone();

		if matches!(self.mode(), EntryMode::Tree) {
			lhs.push('/')
		};

		let mut rhs = other.name.clone();

		if matches!(other.mode(), EntryMode::Tree) {
			rhs.push('/')
		};

		lhs.cmp(&rhs)
	}
}

impl PartialOrd for Entry {
	fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
		Some(self.cmp(other))
	}
}

//NOTE: should be this replaces with a derive?
impl PartialEq for Entry {
	fn eq(&self, other: &Self) -> bool {
		self.mode.eq(&other.mode) && self.oid.eq(&other.oid) && self.name().eq(&other.name())

		/*if self.parent().is_none() {
			self.mode.eq(&other.mode) && self.oid.eq(&other.oid) && self.name().eq(&other.name())
		} else {
			self.mode.eq(&other.mode)
				&& self.oid.eq(&other.oid)
				&& self.name().eq(&other.name())
				&& self.parent().eq(&other.parent())
		}*/
	}
}

impl Entry {
	pub fn mode(&self) -> EntryMode {
		self.mode
	}

	pub fn oid(&self) -> Oid {
		self.oid.clone()
	}

	pub fn name(&self) -> String {
		self.name.clone()
		// FileName::file_name(&self.path.relative_path()).unwrap()
	}

	/*pub fn parent(&self) -> Option<PathBuf> {
		Parent::parent(&self.path.relative_path())
	}*/

	pub fn is_tree(&self) -> bool {
		self.mode.eq(&EntryMode::Tree)
	}

	/*pub fn is_in_root(&self) -> bool {
		Parent::parent(&self.path.relative_path()).is_none()
	}*/

	/*pub fn path(&self) -> Git2PPath {
		self.path.to_owned()
	}*/
}

git2p_git_serializers::serialize_impl!(Entry, Vec<u8>, 'ser);

impl From<&Entry> for Vec<u8> {
	fn from(e: &Entry) -> Self {
		[
			Serialize::serialize(&e.mode()).as_slice(),
			&[SPACE],
			e.name().as_bytes(),
			&[NULL],
			Serialize::serialize(&e.oid()),
		]
		.concat()
	}
}

git2p_git_serializers::deserialize_impl!(Entry, &'de [u8], Error, 'de);

impl TryFrom<&[u8]> for Entry {
	type Error = Error;

	fn try_from(value: &[u8]) -> Result<Self> {
		log::debug!("Entry::try_from");

		let (info, oid) = value.split_by(&NULL)?;
		let oid = Oid::try_deserialize(&oid)?;
		log::debug!("OID: {oid:x}");

		let (mode, name) = info.split_by(&SPACE)?;

		let name = name.as_str()?.to_owned();
		log::debug!("NAME: {name}");
		let mode = EntryMode::try_deserialize(&mode)?;
		log::debug!("MODE: {mode}");

		Ok(Self { mode, oid, name })
	}
}

impl TryFrom<&&[u8]> for Entry {
	type Error = Error;

	fn try_from(value: &&[u8]) -> Result<Self> {
		Self::try_from(*value)
	}
}
