/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{error::Error, result::Result};
use git2p_git_helpers::convert::StrArrayTo;
#[cfg(target_family = "unix")]
use git2p_git_path::is_executable::IsExecutable;
use git2p_git_serializers::{de::TryDeserialize, ser::Serialize};
use serde::{Deserialize, Serialize as Ser};
use std::{fmt::Display, path::PathBuf};

#[derive(Clone, Copy, Debug, Eq, PartialEq, PartialOrd, Ord, Ser, Deserialize)]
pub enum EntryMode {
	Blob = 100644,
	Tree = 40000,
	Executable = 100755,
	Symblink = 120000,
	Submodule = 160000,
}

impl Display for EntryMode {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{:06?}", *self as i32)
	}
}

#[cfg(not(target_arch = "wasm32"))]
impl From<PathBuf> for EntryMode {
	fn from(p: PathBuf) -> Self {
		Self::from(&p)
	}
}

#[cfg(not(target_arch = "wasm32"))]
impl From<&PathBuf> for EntryMode {
	fn from(p: &PathBuf) -> Self {
		let md = git2p_git_path::metadata::metadata(&p);

		if md.is_symlink() {
			EntryMode::Symblink
		} else if md.is_dir() {
			EntryMode::Tree
		} else if p.is_executable() {
			EntryMode::Executable
		} else {
			EntryMode::Blob
		}
	}
}

git2p_git_serializers::serialize_impl!(EntryMode, Vec<u8>, 'ser);

impl From<&EntryMode> for Vec<u8> {
	fn from(e: &EntryMode) -> Self {
		let e = format!("{:?}", *e as i32);
		e.as_bytes().to_owned()
	}
}

git2p_git_serializers::deserialize_impl!(EntryMode, &'de [u8], Error, 'de);

impl TryFrom<&[u8]> for EntryMode {
	type Error = Error;

	fn try_from(v: &[u8]) -> Result<Self> {
		let v = v.str_arr_to::<i32>()?;

		log::debug!("ENTRY MODE: {v}");
		match v {
			x if x == Self::Blob as i32 => Ok(Self::Blob),
			x if x == Self::Tree as i32 => Ok(Self::Tree),
			x if x == Self::Executable as i32 => Ok(Self::Executable),
			x if x == Self::Symblink as i32 => Ok(Self::Symblink),
			x if x == Self::Submodule as i32 => Ok(Self::Submodule),
			_ => Err(Error::BadDeserializationInput),
		}
	}
}

impl TryFrom<&&[u8]> for EntryMode {
	type Error = Error;

	fn try_from(value: &&[u8]) -> Result<Self> {
		Self::try_from(*value)
	}
}
