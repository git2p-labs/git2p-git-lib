/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
use crate::{entry::Entry, error::Error, result::Result};
use git2p_git_helpers::ascii::NULL;
use git2p_git_object::{
	kind::ObjectKind,
	object::{Header, Object},
};
use git2p_git_serializers::{de::TryDeserialize, ser::Serialize};
use serde::{Deserialize, Serialize as Ser};

#[derive(Clone, Debug, Eq, PartialEq, Ser, Deserialize)]
pub struct Tree {
	entries: Vec<Entry>,
}

impl Object<'_, Error> for Tree {
	fn header(&self) -> Header {
		Header::new(ObjectKind::Tree, Serialize::serialize(self).len())
	}

	fn serialized(&self) -> Vec<u8> {
		[
			self.header().serialize().as_slice(),
			&[NULL],
			Serialize::serialize(self).as_slice(),
		]
		.concat()
	}

	fn from_raw(raw: &[u8]) -> Result<Self> {
		Self::try_deserialize(&raw)
	}
}

impl Tree {
	pub fn entries(&self) -> Vec<Entry> {
		self.entries.clone()
	}
}

git2p_git_serializers::serialize_impl!(Tree, Vec<u8>, 'ser);

impl From<&Tree> for Vec<u8> {
	fn from(value: &Tree) -> Self {
		value
			.entries
			.iter()
			.flat_map(|e| Serialize::serialize(e))
			.collect()
	}
}

impl From<Tree> for Vec<u8> {
	fn from(value: Tree) -> Self {
		Vec::from(&value)
	}
}

git2p_git_serializers::deserialize_impl!(Tree, &'de [u8], Error, 'de);

impl TryFrom<&&[u8]> for Tree {
	type Error = Error;

	fn try_from(value: &&[u8]) -> Result<Self> {
		let (mut items, mut buf) = (vec![], vec![]);

		let (mut idx, mut on_hash) = (0usize, false);

		value.iter().enumerate().for_each(|(_, b)| {
			buf.push(*b);

			if b.eq(&NULL) && !on_hash {
				on_hash = true;
				idx = 0usize;
			}

			if idx.eq(&20usize) && on_hash {
				on_hash = false;
				items.push(buf.clone());
				buf = vec![];
			}

			idx += 1;
		});

		let mut entries = vec![];

		items.into_iter().try_for_each(|e| -> Result<()> {
			let entry = Entry::try_deserialize(&&e[..])?;
			log::debug!("Entry to push: {:?}", entry);
			entries.push(entry);
			Ok(())
		})?;

		entries.sort();

		Ok(Self { entries })
	}
}
