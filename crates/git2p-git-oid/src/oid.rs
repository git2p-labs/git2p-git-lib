/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::result::Result;
use git2p_git_helpers::convert::AsStr;
use git2p_git_serializers::de::TryDeserialize;
use serde::{Deserialize, Serialize as Ser};
use std::{
	fmt::{Display, LowerHex},
	ops::Deref,
};

pub const SHA1_BITS_LENGTH: usize = 160;
pub const SHA256_BITS_LENGTH: usize = 256;

// 20 bytes
pub const SHA1_BYTES_LENGTH: usize = SHA1_BITS_LENGTH / 8;
// 32 bytes
pub const SHA256_BYTES_LENGTH: usize = SHA256_BITS_LENGTH / 8;

#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, Ser, Deserialize)]
pub enum Kind {
	#[default]
	Sha1,
	Sha256,
}

#[derive(Clone, Copy, PartialEq, Eq, Ser, Deserialize)]
pub enum Oid {
	Sha1([u8; SHA1_BYTES_LENGTH]),
	Sha256([u8; SHA256_BYTES_LENGTH]),
}

impl Oid {
	pub fn from_str_arr(arr: &[u8]) -> Result<Self> {
		Self::try_deserialize(&arr.as_str()?.to_owned())
	}

	pub fn kind(&self) -> Kind {
		match self {
			Self::Sha1(_) => Kind::Sha1,
			Self::Sha256(_) => Kind::Sha256,
		}
	}

	pub fn zero_id(kind: Kind) -> Self {
		match kind {
			Kind::Sha1 => Self::Sha1([0u8; SHA1_BYTES_LENGTH]),
			Kind::Sha256 => Self::Sha256([0u8; SHA256_BYTES_LENGTH]),
		}
	}

	pub fn zero_id_sha1() -> Self {
		Self::zero_id(Kind::Sha1)
	}

	pub fn zero_id_sha256() -> Self {
		Self::zero_id(Kind::Sha256)
	}

	pub fn is_sha1(&self) -> bool {
		matches!(self.kind(), Kind::Sha1)
	}

	pub fn is_sha256(&self) -> bool {
		!self.is_sha1()
	}

	pub fn is_zero_id(&self) -> bool {
		match self {
			Oid::Sha1(oid) => oid == &[0u8; SHA1_BYTES_LENGTH],
			Oid::Sha256(oid) => oid == &[0u8; SHA256_BYTES_LENGTH],
		}
	}
}

impl std::fmt::Debug for Oid {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			Self::Sha1(h) => write!(f, "SHA-1: [{}, .., {}]", h[0], h[19]),
			Self::Sha256(h) => write!(f, "SHA-256: [{}, .., {}]", h[0], h[31]),
		}
	}
}

impl Deref for Oid {
	type Target = [u8];

	fn deref(&self) -> &Self::Target {
		match self {
			Oid::Sha1(h) => &h[..],
			Oid::Sha256(h) => &h[..],
		}
	}
}

impl LowerHex for Oid {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		let s = &*self.iter().map(|b| format!("{b:02x}")).collect::<String>();

		write!(f, "{s}")
	}
}

impl Display for Oid {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{self:x}")
	}
}
