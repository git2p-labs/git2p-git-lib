/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{error::Error, oid::Oid, result::Result};
use git2p_git_serializers::de::TryDeserialize;
use std::num::ParseIntError;

git2p_git_serializers::deserialize_impl!(Oid, &'de [u8], Error, 'de);

impl<'de> TryFrom<&'de &'de [u8]> for Oid {
	type Error = Error;

	fn try_from(v: &'de &'de [u8]) -> Result<Self> {
		Self::try_from(*v)
	}
}

impl<'de> TryFrom<&'de [u8]> for Oid {
	type Error = Error;

	fn try_from(v: &'de [u8]) -> Result<Self> {
		if v.len().eq(&20usize) {
			Ok(Self::Sha1(v.try_into().unwrap()))
		} else if v.len().eq(&32usize) {
			Ok(Self::Sha256(v.try_into().unwrap()))
		} else {
			Err(Error::BadHash)
		}
	}
}

git2p_git_serializers::deserialize_impl!(Oid, String, Error, 'de);

impl TryFrom<&String> for Oid {
	type Error = Error;

	fn try_from(value: &String) -> Result<Self> {
		Self::try_from(value.to_owned())
	}
}

impl TryFrom<String> for Oid {
	type Error = Error;

	fn try_from(v: String) -> Result<Self> {
		let vec: std::result::Result<Vec<u8>, ParseIntError> = (0..v.len())
			.step_by(2)
			.map(|i| u8::from_str_radix(&v[i..i + 2], 16))
			.collect();

		if let Ok(vec) = vec {
			Self::try_from(&vec[..])
		} else {
			Err(Error::BadHash)
		}
	}
}
