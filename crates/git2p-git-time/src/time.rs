/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{error::Error, result::Result};
use git2p_git_helpers::{
	ascii::SPACE,
	convert::{SplitBy, StrArrayTo},
};
use git2p_git_serializers::{de::TryDeserialize, ser::Serialize};
use serde::{Deserialize, Serialize as Ser};
use std::{
	fmt::Display,
	ops::{Deref, DerefMut},
};
use time::{macros::format_description, OffsetDateTime, UtcOffset};

pub trait GitTime: std::fmt::Debug + Clone + Copy + PartialEq + Eq + Ord + PartialOrd {
	fn new(time: i64, offset: (i8, i8, i8)) -> Result<Self>;

	fn seconds(&self) -> i64;

	fn offset_secs(&self) -> i32;

	fn sign(&self) -> char;
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Ser, Deserialize)]
pub struct Time(OffsetDateTime);

impl GitTime for Time {
	fn new(timestamp: i64, (h, m, s): (i8, i8, i8)) -> Result<Self> {
		let datetime = OffsetDateTime::from_unix_timestamp(timestamp)?;

		let offset = UtcOffset::from_hms(h, m, s)?;

		let datetime = datetime.to_offset(offset);

		Ok(Time(datetime))
	}

	fn seconds(&self) -> i64 {
		self.0.unix_timestamp()
	}

	fn offset_secs(&self) -> i32 {
		self.0.offset().whole_seconds()
	}

	fn sign(&self) -> char {
		self.0
			.offset()
			.is_positive()
			.then_some('+')
			.map_or('-', |s| s)
	}
}

impl AsRef<OffsetDateTime> for Time {
	fn as_ref(&self) -> &OffsetDateTime {
		&self.0
	}
}

impl AsMut<OffsetDateTime> for Time {
	fn as_mut(&mut self) -> &mut OffsetDateTime {
		&mut self.0
	}
}

impl Deref for Time {
	type Target = OffsetDateTime;

	fn deref(&self) -> &Self::Target {
		&self.0
	}
}

impl DerefMut for Time {
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.0
	}
}

impl Time {
	pub fn now_local() -> Result<Self> {
		let datetime = OffsetDateTime::now_local()?.replace_millisecond(0)?;

		Ok(Self(datetime))
	}

	pub fn now_utc() -> Result<Self> {
		Ok(Self(OffsetDateTime::now_utc().replace_millisecond(0)?))
	}

	pub fn format_ts_tz(&self) -> String {
		let ts = self.0.unix_timestamp();

		let (h, m, _) = self.0.offset().as_hms();

		let h = if self.0.offset().is_positive() {
			format!("+{h:02}")
		} else {
			format!("{h:03}")
		};

		format!("{ts} {h}{m:02}")
	}
}

git2p_git_serializers::serialize_impl!(Time, Vec<u8>, 'de);

impl From<&Time> for Vec<u8> {
	fn from(value: &Time) -> Self {
		value.format_ts_tz().as_bytes().to_owned()
	}
}

git2p_git_serializers::deserialize_impl!(Time, &'de [u8], Error, 'de);

impl TryFrom<&&[u8]> for Time {
	type Error = Error;

	fn try_from(value: &&[u8]) -> Result<Self> {
		let (ts, of) = value.split_by(&SPACE)?;

		let ts = ts.str_arr_to::<i64>()?;

		let h = &of[0..3];
		let h = h.str_arr_to::<i8>()?;

		let m = &of[3..5];
		let m = m.str_arr_to::<i8>()?;

		Self::new(ts, (h, m, 0))
	}
}

impl Display for Time {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		let format = format_description!("[month repr:short] [weekday repr:short] [day] [hour]:[minute]:[second] [year] [offset_hour sign:mandatory][offset_minute]");

		let s = (**self)
			.format(format)
			.map_err(|_| std::fmt::Error::default())?;

		write!(f, "{s}")
	}
}

#[test]
fn serialization() {
	let now = Time::now_local().map_or(Time::now_utc().unwrap(), |t| t);

	let serialized = git2p_git_serializers::ser::Serialize::serialize(&now);

	let deserialized = Time::try_deserialize(&&serialized[..]).unwrap();

	assert_eq!(now, deserialized)
}
