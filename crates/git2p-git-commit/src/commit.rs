/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{error::Error, result::Result};
use git2p_git_author::author::Author;
use git2p_git_helpers::{
	ascii::{LF, NULL, SPACE},
	convert::{AsStr, SplitAll},
};
use git2p_git_object::{
	kind::ObjectKind,
	object::{Header, Object},
};
use git2p_git_oid::oid::Oid;
use git2p_git_serializers::{de::TryDeserialize, ser::Serialize};
use serde::{Deserialize, Serialize as Ser};

#[derive(Debug, Clone, PartialEq, Eq, Ser, Deserialize)]
pub struct Commit {
	tree: Oid,
	parents: Vec<Oid>,
	author: Author,
	committer: Author,
	gpgsig: Option<Vec<u8>>,
	message: String,
}

impl Object<'_, Error> for Commit {
	fn header(&self) -> Header {
		Header::new(ObjectKind::Commit, Serialize::serialize(self).len())
	}

	fn serialized(&self) -> Vec<u8> {
		[
			self.header().serialize().as_slice(),
			&[NULL],
			Serialize::serialize(self).as_slice(),
		]
		.concat()
	}

	fn from_raw(raw: &[u8]) -> std::result::Result<Self, Error> {
		Self::try_deserialize(&raw)
	}
}

impl Commit {
	pub fn tree(&self) -> Oid {
		self.tree.clone()
	}

	pub fn parents(&self) -> Vec<Oid> {
		self.parents.clone()
	}

	pub fn author(&self) -> Author {
		self.author.clone()
	}

	pub fn committer(&self) -> Author {
		self.committer.clone()
	}

	pub fn gpgsig(&self) -> Option<Vec<u8>> {
		self.gpgsig.clone()
	}

	pub fn message(&self) -> String {
		self.message.clone()
	}

	pub fn is_signed(&self) -> bool {
		self.gpgsig.is_some()
	}
}

git2p_git_serializers::serialize_impl!(Commit, Vec<u8>, 'ser);

impl From<&Commit> for Vec<u8> {
	fn from(value: &Commit) -> Self {
		let sp = &[SPACE];
		let lf = &[LF];

		let parents = value
			.parents
			.iter()
			.flat_map(|oid| [b"parent".as_slice(), sp, oid.to_string().as_bytes(), lf].concat())
			.collect::<Vec<_>>();

		let gpgsig = if let Some(gpg) = &value.gpgsig {
			if value.committer().name().eq("GitHub") {
				// wft??????????????????????????
				[b"gpgsig".as_slice(), sp, gpg.as_slice(), lf, sp].concat()
			} else {
				[b"gpgsig".as_slice(), sp, gpg.as_slice(), lf].concat()
			}
		} else {
			vec![]
		};

		[
			b"tree".as_slice(),
			sp,
			value.tree.to_string().as_bytes(),
			lf,
			&parents,
			b"author".as_slice(),
			sp,
			&Serialize::serialize(&value.author),
			lf,
			b"committer".as_slice(),
			sp,
			&Serialize::serialize(&value.committer),
			lf,
			&gpgsig,
			lf,
			value.message.as_bytes(),
		]
		.concat()
	}
}

impl From<Commit> for Vec<u8> {
	fn from(value: Commit) -> Self {
		Vec::from(&value)
	}
}

git2p_git_serializers::deserialize_impl!(Commit, &'de [u8], Error, 'de);

impl TryFrom<&&[u8]> for Commit {
	type Error = Error;

	fn try_from(value: &&[u8]) -> Result<Self> {
		let lines = value.split_all(&LF)?;

		let mut deserializer = CommitDeserializer::new(lines)?;

		deserializer.deserialize(value)
	}
}

struct CommitDeserializer<'de> {
	lines: Vec<&'de [u8]>,
	is_signed: bool,
}

impl<'de> CommitDeserializer<'de> {
	fn new(lines: Vec<&'de [u8]>) -> Result<Self> {
		let mut is_signed = false;

		for line in &lines {
			if line.starts_with(b"gpgsig ") {
				is_signed = true;
				break;
			}
		}

		Ok(Self { lines, is_signed })
	}

	fn deserialize(&mut self, raw: &'de [u8]) -> Result<Commit> {
		let tree = self.deserialize_tree()?;

		let parents = self.deserialize_parents()?;

		let author = self.deserialize_author()?;

		let committer = self.deserialize_commiter()?;

		let gpgsig = self.deserialize_gppsig()?;

		let message = self.deserialize_message(raw)?;

		Ok(Commit {
			tree,
			parents,
			author,
			committer,
			gpgsig,
			message,
		})
	}

	fn deserialize_tree(&mut self) -> Result<Oid> {
		Ok(Oid::from_str_arr(
			&self.lines[0][self.lines[0].len() - 40..],
		)?)
	}

	fn deserialize_parents(&mut self) -> Result<Vec<Oid>> {
		let mut parents = vec![];

		for line in &self.lines {
			if line.starts_with(b"author ") {
				break;
			}

			if line.starts_with(b"parent ") {
				let parent = Oid::from_str_arr(&line[line.len() - 40..])?;
				parents.push(parent);
			}
		}

		Ok(parents)
	}

	fn deserialize_author(&mut self) -> Result<Author> {
		for line in &self.lines {
			if line.starts_with(b"author ") {
				let author = Author::try_deserialize(&&line[7..])?;
				return Ok(author);
			}
		}
		Err(Error::BadDeserializationInput)
	}

	fn deserialize_commiter(&mut self) -> Result<Author> {
		for line in &self.lines {
			if line.starts_with(b"committer ") {
				let author = Author::try_deserialize(&&line[10..])?;
				return Ok(author);
			}
		}
		Err(Error::BadDeserializationInput)
	}

	fn deserialize_gppsig(&mut self) -> Result<Option<Vec<u8>>> {
		if !self.is_signed {
			return Ok(None);
		}

		let (mut start, mut end) = (0usize, 0usize);

		for (idx, line) in self.lines.iter().enumerate() {
			if line.starts_with(b"gpgsig ") {
				start = idx;
			}

			if line.starts_with(b" -----END PGP SIGNATURE-----") {
				end = idx;
				break;
			}
		}

		let mut sig = Vec::new();

		for (idx, line) in self.lines[start..=end].iter().enumerate() {
			if idx == 0 {
				sig.extend_from_slice(&line[7..]);
			} else {
				sig.extend_from_slice(line);
			}
			if idx + start != end {
				sig.push(b'\n');
			}
		}

		Ok(Some(sig))
	}

	fn deserialize_message(&mut self, raw: &'de [u8]) -> Result<String> {
		let mut start = 0usize;

		for (idx, line) in self.lines.iter().enumerate() {
			let prefix = if self.is_signed {
				b" -----END PGP SIGNATURE-----".as_slice()
			} else {
				b"committer ".as_slice()
			};

			if line.starts_with(prefix) {
				start = idx + 1;
				break;
			}
		}

		let padding = self.lines[..=start]
			.iter()
			.map(|l| l.len())
			.fold(start + 1, |acc, x| acc + x);

		// here is the problem why i need pass the raw value to deserializer
		// split_all consumes pattern and this affects to commit msg
		let msg = raw[padding..].iter().copied().collect::<Vec<_>>();

		let msg = msg.as_str()?;

		Ok(msg.to_owned())
	}
}

#[test]
fn signed_root() {
	//commit 420
	let raw_inner = "tree befcaeffcc3bf3cb75dcae6f164f590b6a9c430f
author 04l3x <alex.vacan.bala@gmail.com> 1698004516 -0500
committer 04l3x <alex.vacan.bala@gmail.com> 1698004516 -0500
gpgsig -----BEGIN PGP SIGNATURE-----
\u{0020}
 iHUEABEIAB0WIQRuepMRaMXGokt408ibarGUxKn2+wUCZTV+JAAKCRCbarGUxKn2
 +/MbAP9M3DR8wPpljWenx+OBBMDvV3x2vx3qlKrJQPEsWD7MWgD/WfjO9MdN61je
 wQPCkbXjBYcpAiw6dKAGaQvbtf5yzTg=
 =jhtc
 -----END PGP SIGNATURE-----

first commit\n"
		.as_bytes();

	let expected_oid = b"d3744fee03189e382783fa2433e5aef78520ee09";
	let expected_oid = Oid::from_str_arr(expected_oid).unwrap();

	let commit = Commit::try_deserialize(&raw_inner).unwrap();

	let oid = commit.oid().unwrap();

	assert_eq!(expected_oid, oid);
}

#[test]
fn signed_no_root() {
	//commit 486
	let raw_inner = "tree fe1cbaa2b8f31cb660b054ca080882a74037d3f2
parent 8cdc20f54ffdc0b1cfb70757961e6432bba7dbb0
author 04l3x <alex.vacan.bala@gmail.com> 1698090773 -0500
committer 04l3x <alex.vacan.bala@gmail.com> 1698090773 -0500
gpgsig -----BEGIN PGP SIGNATURE-----
\u{0020}
 iHUEABEIAB0WIQRuepMRaMXGokt408ibarGUxKn2+wUCZTbPFQAKCRCbarGUxKn2
 +zCxAP9jd9k1nsSCJ0ihNpAfsX1AEajUWTOMN78PJ1GFKMDf4QD+PMfhRwQqWsGC
 0Zoa7jfHNdArQKg3hGFtLupMAEaDyVU=
 =NKou
 -----END PGP SIGNATURE-----

separate git api and git logic\n"
		.as_bytes();

	let expected_oid = b"151d7cd54836fc14afbfadc58ffbbb5093b91839";
	let expected_oid = Oid::from_str_arr(expected_oid).unwrap();

	let commit = Commit::try_deserialize(&raw_inner).unwrap();

	let oid = commit.oid().unwrap();

	assert_eq!(expected_oid, oid);
}

#[test]
fn signed_external_committer() {
	//commit 795
	let raw_inner = "tree bc86d157a65f23f89ee2269e5616dd9836285027
parent e106ef63e913f0e367875050c87694287f4273f1
parent fc1ec1bc840c64853365357cb28300eb708c6390
author al3x <100656494+04l3x@users.noreply.github.com> 1667057100 -0500
committer GitHub <noreply@github.com> 1667057100 -0500
gpgsig -----BEGIN PGP SIGNATURE-----
\u{0020}
 wsBcBAABCAAQBQJjXUXMCRBK7hj4Ov3rIwAA9qoIAABz0TPNqFTG6ZBvUNO6Gjva
 /ohq1feXZonbY0qAZ4gF8MYlXCxlBcDW9UzTV3JXNrIntvOjF14reif+h+OR7NET
 NlXvZRCVAIlhaVvRpw+McZREOB0vyaab2huzyPPQ8bARAleLOHiIWb+8BFXyxrxm
 IXmLQgebf9KXeCeWLw49lvxwI2l4rB8OZ2QoilNOWO89t1UEaQIeGX6g1M/UlXsj
 VqQgZDVwYVLmmGtqHtPoRipowTGkeGGXgkayTknupoh6Wtzp7ar8SiwcxqAcawot
 7rTcyWvkU6GyCMzezMFvz0sWYpte64klqeG5w3GofKm3VPSPHl0qL2WUyQ+NQUc=
 =Mvne
 -----END PGP SIGNATURE-----


Merge pull request #5 from 04l3x/master

Update README"
		.as_bytes();

	let expected_oid = b"a2bf18a166741dcc51f4511fbdd21632a9083a25";
	let expected_oid = Oid::from_str_arr(expected_oid).unwrap();

	let commit = Commit::try_deserialize(&raw_inner).unwrap();

	let oid = commit.oid().unwrap();

	assert_eq!(expected_oid, oid);
}

#[test]
fn no_signed_root() {
	//commit 226
	let raw_inner = "tree a27694157848fc56d8075b37deed4314a3836a60
author 04l3x <alex.vacan.bala@gmail.com> 1665781949 -0500
committer 04l3x <alex.vacan.bala@gmail.com> 1665781949 -0500

first commit; basic color mapping, and basic working scheme\n"
		.as_bytes();

	let expected_oid = b"b7ed68ced1503b5cba94dd1c836703cc952b9cef";
	let expected_oid = Oid::from_str_arr(expected_oid).unwrap();

	let commit = Commit::try_deserialize(&raw_inner).unwrap();

	let oid = commit.oid().unwrap();

	assert_eq!(expected_oid, oid);
}

#[test]
fn no_signed_no_root() {
	//commit 306
	let raw_inner = "tree dce88f722398c75cd069755c887a41aad83597f6
parent 2f1445e05ca8c475257b96d7ce8b9c5495c4b13c
author 04l3x <alex.vacan.bala@gmail.com> 1666917965 -0500
committer 04l3x <alex.vacan.bala@gmail.com> 1666917965 -0500

feat: basic shape system. fix: color system and theme provider is working on. update reamde\n"
		.as_bytes();

	let expected_oid = b"ed7b87aec5e5a7f5e9ee21175e60ad376dacf21e";
	let expected_oid = Oid::from_str_arr(expected_oid).unwrap();

	let commit = Commit::try_deserialize(&raw_inner).unwrap();

	let oid = commit.oid().unwrap();

	assert_eq!(expected_oid, oid);
}
