/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::xdelta::{Delta, Deltas, BLOCK_SIZE};
use std::{
	collections::{hash_map::DefaultHasher, HashMap},
	hash::{Hash, Hasher},
};

pub struct XDeltaEncoder {
	source: Vec<u8>,
	index: HashMap<u64, Vec<usize>>,
	target_offset: usize,
	insert_buf: Vec<u8>,
	deltas: Deltas,
}

impl XDeltaEncoder {
	pub fn new(source: Vec<u8>) -> Self {
		let mut index = HashMap::new();
		let blocks = source.len() / BLOCK_SIZE;

		for i in 0..blocks {
			let offset = i * BLOCK_SIZE;

			let mut hasher = DefaultHasher::new();

			Hash::hash_slice(&source[offset..offset + BLOCK_SIZE], &mut hasher);

			index
				.entry(hasher.finish())
				.and_modify(|o: &mut Vec<_>| o.push(offset))
				.or_insert(vec![offset]);
		}

		Self {
			source,
			index,
			target_offset: 0,
			insert_buf: Vec::new(),
			deltas: Deltas(Vec::new()),
		}
	}

	// returns the list of matches in target at x index against source_index
	fn find_matches(&mut self, target: &[u8]) -> Vec<Match> {
		if self.target_offset + BLOCK_SIZE > target.len() {
			return vec![];
		}

		let slice = &target[self.target_offset..self.target_offset + BLOCK_SIZE];

		let mut hasher = DefaultHasher::new();

		Hash::hash_slice(slice, &mut hasher);

		let hash = hasher.finish();

		let Some(offset_match_list) = self.index.get(&hash) else {
			return vec![];
		};

		offset_match_list
			.iter()
			.cloned()
			.map(|off| Match {
				source_offset: off,
				target_offset: self.target_offset,
				len: BLOCK_SIZE,
			})
			.collect()
	}

	// tries to expand a [Match](Match) as much as possible without altering
	// [Encoder](XDeltaEncoder)'s state
	fn expand_match(&mut self, target_match: &mut Match, target: &[u8]) {
		self.expand_to_left(target_match, target);
		self.expand_to_right(target_match, target);
	}

	fn expand_to_left(&mut self, target_match: &mut Match, target: &[u8]) {
		while self.source[target_match.source_offset - 1] == target[target_match.target_offset - 1]
		{
			target_match.source_offset -= 1;
			target_match.target_offset -= 1;
			target_match.len += 1;
		}
	}

	fn expand_to_right(&mut self, target_match: &mut Match, target: &[u8]) {
		let mut source_off = target_match.source_offset + target_match.len;
		let mut target_off = target_match.target_offset + target_match.len;

		while self.source[source_off] == target[target_off] {
			source_off += 1;
			target_off += 1;
			target_match.len += 1;
		}
	}

	pub fn encode(&mut self, target: Vec<u8>) -> Deltas {
		self.reset();

		while self.target_offset < target.len() {
			let mut matches = self.find_matches(&target);

			if matches.is_empty() {
				self.insert_buf.push(target[self.target_offset]);
				self.target_offset += 1;
				continue;
			}

			matches
				.iter_mut()
				.for_each(|m| self.expand_match(m, &target));

			matches.sort();

			let max = matches.last().unwrap();

			if !self.insert_buf.is_empty() {
				self.deltas.as_mut().push(Delta::Insert {
					data: self.insert_buf[..max.source_offset - 1].to_vec(),
				});
				self.insert_buf.clear();
			}

			self.deltas.as_mut().push(Delta::Copy {
				offset: max.source_offset,
				len: max.len,
			});

			self.target_offset = max.target_offset + max.len;
		}

		if !self.insert_buf.is_empty() {
			self.deltas.as_mut().push(Delta::Insert {
				data: self.insert_buf.to_owned(),
			});
		};

		self.deltas.to_owned()
	}

	fn reset(&mut self) {
		self.target_offset = 0;
		self.insert_buf.clear();
		self.deltas.as_mut().clear();
	}
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct Match {
	source_offset: usize,
	target_offset: usize,
	len: usize,
}

impl PartialOrd for Match {
	fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
		Some(self.cmp(&other))
	}
}

impl Ord for Match {
	fn cmp(&self, other: &Self) -> std::cmp::Ordering {
		self.len.cmp(&other.len)
	}
}

#[test]
fn test_match_ord() {
	let mut matches = vec![
		Match {
			source_offset: BLOCK_SIZE,
			target_offset: BLOCK_SIZE,
			len: 16,
		},
		Match {
			source_offset: BLOCK_SIZE,
			target_offset: BLOCK_SIZE,
			len: 20,
		},
		Match {
			source_offset: BLOCK_SIZE,
			target_offset: BLOCK_SIZE,
			len: 18,
		},
		Match {
			source_offset: BLOCK_SIZE,
			target_offset: BLOCK_SIZE,
			len: 32,
		},
		Match {
			source_offset: BLOCK_SIZE,
			target_offset: BLOCK_SIZE,
			len: 5,
		},
		Match {
			source_offset: BLOCK_SIZE,
			target_offset: BLOCK_SIZE,
			len: 19,
		},
	];

	matches.sort();

	assert_eq!(
		matches.last(),
		Some(&Match {
			source_offset: BLOCK_SIZE,
			target_offset: BLOCK_SIZE,
			len: 32
		})
	);
}

#[test]
fn encode_test() {
	let source = b"the quick brown fox jumps over the slow lazy dog";
	let target = b"a swift auburn fox jumps over three dormant hounds";

	let mut encoder = XDeltaEncoder::new(source.to_vec());

	let encoded = encoder.encode(target.to_vec());

	println!("{encoded:#?}");

	let expected = vec![
		Delta::Insert {
			data: b"a swift aubur".to_vec(),
		},
		Delta::Copy {
			offset: 14,
			len: 19,
		},
		Delta::Insert {
			data: b"ree dormant hounds".to_vec(),
		},
	];

	assert_eq!(encoded, Deltas(expected));
}
