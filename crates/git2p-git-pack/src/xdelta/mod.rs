/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use std::fmt::Debug;

pub mod decoder;
pub mod encoder;

pub const BLOCK_SIZE: usize = 16;
pub const MAX_COPY_SIZE: u32 = 0xFFFFFF;
pub const MAX_INSERT_SIZE: u8 = 0x7F;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Deltas(Vec<Delta>);

impl From<&[u8]> for Deltas {
	// takes the slice of delta data that contains the deltas
	// the first bytes of the slice is the header of firt delta per se
	fn from(value: &[u8]) -> Self {
		let mut deltas = vec![];
		let mut cursor = 0usize;

		loop {
			let delta_header = value[cursor];

			let delta_size = if delta_header < 0x80 {
				delta_header as usize
			} else {
				(delta_header.count_ones() - 1) as usize
			};

			let delta = Delta::from(&value[cursor..cursor + delta_size + 1]);
			deltas.push(delta);

			if cursor + delta_size + 1 >= value.len() {
				break;
			}

			cursor += delta_size + 1;
		}

		Deltas(deltas)
	}
}

impl AsRef<Vec<Delta>> for Deltas {
	fn as_ref(&self) -> &Vec<Delta> {
		&self.0
	}
}

impl AsMut<Vec<Delta>> for Deltas {
	fn as_mut(&mut self) -> &mut Vec<Delta> {
		&mut self.0
	}
}

#[derive(Clone, PartialEq, Eq)]
pub enum Delta {
	Copy { offset: usize, len: usize },

	Insert { data: Vec<u8> },
}

impl Delta {
	pub fn serialize(&self) -> Vec<u8> {
		Vec::from(self)
	}

	pub fn deserialize(bytes: &[u8]) -> Self {
		Delta::from(bytes)
	}
}

impl From<&[u8]> for Delta {
	// PERFOMANCE: i should do this with bitwise operations
	// but i really need see some results quickly!!!!!
	fn from(value: &[u8]) -> Self {
		let header_byte = value[0];

		if header_byte < 0x80 {
			Delta::Insert {
				data: value[1..].to_vec(),
			}
		} else {
			let active_array = format!("{header_byte:b}");

			let mut active_array = active_array.as_bytes().to_vec();
			active_array.reverse();

			let active_array: [u8; 7] = active_array[0..7].try_into().unwrap();

			let mut offset = [0u8; 8];

			let mut idx = 1usize;

			for i in 0usize..4usize {
				if active_array[i] == 49 {
					offset[i] = value[idx];
					idx += 1;
				}
			}

			let offset = usize::from_le_bytes(offset);

			let mut len = [0u8; 8];

			for i in 4usize..7usize {
				if active_array[i] == 49 {
					len[i - 4] = value[idx];
					idx += 1;
				}
			}

			let len = usize::from_le_bytes(len);

			Delta::Copy { offset, len }
		}
	}
}

#[test]
fn serialization_at_limit_test() {
	let delta_copy = Delta::Copy {
		offset: u32::MAX as usize,
		len: 2usize.pow(24) - 1,
	};

	let copy = Delta::deserialize(&delta_copy.serialize());

	assert_eq!(delta_copy, copy);
}

#[test]
fn serialization_test() {
	let delta_insert = Delta::Insert {
		data: b"a swift aubur".to_vec(),
	};

	let delta_copy = Delta::Copy {
		offset: u32::MAX as usize,
		len: 2usize.pow(7) as usize,
	};

	let insert = Delta::deserialize(&delta_insert.serialize());

	assert_eq!(delta_insert, insert);

	let copy = Delta::deserialize(&delta_copy.serialize());

	assert_eq!(delta_copy, copy);
}

impl From<&Delta> for Vec<u8> {
	fn from(delta: &Delta) -> Self {
		match delta {
			// [ ctrl byte | offset  |  len  ]
			// [     -     | - - - - | - - - ]
			Delta::Copy { offset, len } => {
				let mut active_bits = [0u8; 8];
				active_bits[0] = 1;

				let mut len = len.to_be_bytes();

				for i in 5usize..8 {
					if len[i] != 0 {
						active_bits[i - 4] = 1;
					}
				}

				let mut offset = offset.to_be_bytes();

				for i in 4usize..8 {
					if offset[i] != 0 {
						active_bits[i] = 1;
					}
				}

				offset.reverse();

				len.reverse();

				let mut header = 0u8;

				for i in 0usize..8 {
					let exp = 7 - i as u32;
					let val = 2u8.pow(exp) * active_bits[i];
					header += val;
				}

				let mut delta = Vec::new();

				for i in 0usize..4 {
					if offset[i] != 0 {
						delta.push(offset[i]);
					}
				}

				for i in 0usize..3 {
					if len[i] != 0 {
						delta.push(len[i]);
					}
				}
				delta.insert(0, header);

				delta
			}
			Delta::Insert { data } => {
				let len = data.len() as u8;
				[&[len], &data[..]].concat()
			}
		}
	}
}

impl Debug for Delta {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			Delta::Copy { offset, len } => f
				.debug_struct("Copy")
				.field("offset", &offset)
				.field("len", &len)
				.finish(),

			Delta::Insert { data } => f
				.debug_struct("Insert")
				.field("data", &String::from_utf8_lossy(&data))
				.finish(),
		}
	}
}

#[derive(Debug)]
pub struct DeltaData {
	pub base_len: u64,
	pub target_len: u64,
	pub deltas: Deltas,
}

impl From<&[u8]> for DeltaData {
	fn from(value: &[u8]) -> Self {
		let mut cursor = 0usize;

		let mut base_len = vec![];
		loop {
			let byte = value[cursor];
			base_len.push(byte);
			cursor += 1;
			if byte < 0x80 {
				break;
			}
		}
		let base_len = leb128fmt::decode_uint_slice::<u64, 64>(&base_len, &mut 0).expect("");

		let mut target_len = vec![];
		loop {
			let byte = value[cursor];
			target_len.push(byte);
			cursor += 1;
			if byte < 0x80 {
				break;
			}
		}
		let target_len = leb128fmt::decode_uint_slice::<u64, 64>(&target_len, &mut 0).expect("");

		let deltas = &value[cursor..];

		Self {
			base_len,
			target_len,
			deltas: Deltas::from(deltas),
		}
	}
}
