/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::xdelta::{Delta, Deltas};

pub struct XDeltaDecoder<'xdelta> {
	source: &'xdelta [u8],
}

impl<'xdelta> XDeltaDecoder<'xdelta> {
	pub fn new(source: &'xdelta [u8]) -> Self {
		Self { source }
	}

	pub fn decode(&self, deltas: Deltas) -> Vec<u8> {
		let mut decoded = Vec::new();

		for delta in deltas.as_ref() {
			match delta {
				Delta::Copy { offset, len } => {
					decoded.extend_from_slice(&self.source[*offset..*offset + *len])
				}
				Delta::Insert { data } => decoded.extend_from_slice(&data[..]),
			}
		}

		decoded
	}
}

#[test]
fn decode_test() {
	let deltas = vec![
		Delta::Insert {
			data: b"a swift aubur".to_vec(),
		},
		Delta::Copy {
			offset: 14,
			len: 19,
		},
		Delta::Insert {
			data: b"ree dormant hounds".to_vec(),
		},
	];

	let source = b"the quick brown fox jumps over the slow lazy dog";

	let expected = b"a swift auburn fox jumps over three dormant hounds";

	let decoder = XDeltaDecoder::new(source);

	let decoded = decoder.decode(Deltas(deltas));

	println!("{}", String::from_utf8_lossy(&decoded[..]));

	assert_eq!(decoded, expected.to_vec())
}
