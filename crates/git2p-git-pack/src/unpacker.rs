/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{
	error::Error,
	pack::{
		metadata::{self, Entry, Pack},
		EntryHeader, EntryKind, Header,
	},
	result::Result,
	xdelta::{decoder::XDeltaDecoder, DeltaData},
};
use flate2::read::ZlibDecoder;
use git2p_git_object::kind::ObjectKind;
use git2p_git_odb::{
	compressor::Decompress,
	odb::{GitOdbObject, OdbObject},
};
use git2p_git_oid::oid::Oid;
use std::io::Read;

pub struct Unpacker<'pack> {
	header: Header,
	pack: &'pack [u8],
	cursor: usize,
}

impl<'pack> Unpacker<'pack> {
	pub fn new(pack: &'pack [u8]) -> Result<Self> {
		let header = Header::try_from(&pack[..12])?;

		Ok(Self {
			header,
			pack,
			cursor: 0,
		})
	}

	pub fn resolve_delta_ref(&self, base: OdbObject, entry: Entry) -> Result<OdbObject> {
		let delta_data = &self.pack[entry.header_filled_bytes + entry.offset + 20..];
		let delta_data = delta_data.decompress()?;
		let delta_data = DeltaData::from(&delta_data[..]);

		let source = base.data();

		assert_eq!(delta_data.base_len as usize, source.len());

		let decoder = XDeltaDecoder::new(&source);

		let target = decoder.decode(delta_data.deltas);

		assert_eq!(delta_data.target_len as usize, target.len());

		let object = OdbObject::try_from_packed(&target, base.kind())?;

		Ok(object)
	}

	pub fn resolve_delta_ofs(&self, _: Entry) -> Result<OdbObject> {
		todo!()
	}

	pub fn unpack_undeltified(&self, entry: Entry) -> Result<OdbObject> {
		let kind = ObjectKind::try_from(entry.kind)?;

		let obj = &self.pack[entry.header_filled_bytes + entry.offset..];
		let obj = obj.decompress()?;

		let obj = OdbObject::try_from_packed(&obj[..], kind).expect("error converting odb");

		Ok(obj)
	}

	pub fn pack_metadata(&mut self) -> Result<Pack> {
		let Ok(checksum) = Oid::try_from(&self.pack[self.pack.len() - 20..]) else {
			return Err(Error::InvalidChecksum);
		};
		let mut metadata = Pack::new(self.header, checksum);
		self.cursor = 12;

		loop {
			log::trace!("deserializing entry...");
			let header = EntryHeader::from(&self.pack[self.cursor..]);

			let (mut decoder, kind) = match header.kind {
				EntryKind::RefDelta => {
					let ref_id = &self.pack[self.cursor + header.filled_bytes()
						..self.cursor + header.filled_bytes() + 20];

					let Ok(ref_id) = Oid::try_from(ref_id) else {
						return Err(Error::InvalidPackEntry);
					};

					(
						ZlibDecoder::new(&self.pack[self.cursor + header.filled_bytes() + 20..]),
						metadata::EntryKind::ref_delta(ref_id),
					)
				}
				_ => (
					ZlibDecoder::new(&self.pack[self.cursor + header.filled_bytes()..]),
					metadata::EntryKind::from(header.kind),
				),
			};

			let mut object = Vec::new();

			let _ = decoder.read_to_end(&mut object).expect("");

			let entry = Entry {
				kind,
				header_filled_bytes: header.filled_bytes(),
				offset: self.cursor,
				compressed_size: decoder.total_in(),
				decompressed_size: decoder.total_out(),
			};

			metadata.entry_list.as_mut().push(entry);

			match header.kind {
				EntryKind::RefDelta => {
					self.cursor += decoder.total_in() as usize + header.filled_bytes() + 20
				}
				_ => self.cursor += decoder.total_in() as usize + header.filled_bytes(),
			};

			if self.cursor >= self.pack.len() - 20 {
				break;
			}
		}

		Ok(metadata)
	}
}
