/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{error::Error, pack::Header, result::Result};
use git2p_git_object::kind::ObjectKind;
use git2p_git_oid::oid::Oid;
use std::slice::Iter;

#[derive(Debug, Clone)]
pub struct Pack {
	pub header: Header,
	pub entry_list: EntryList,
	pub checksum: Oid,
}

impl Pack {
	pub fn new(header: Header, checksum: Oid) -> Self {
		Self {
			header,
			entry_list: EntryList::default(),
			checksum,
		}
	}
}

#[derive(Debug, Default, Clone)]
pub struct EntryList(Vec<Entry>);

impl EntryList {
	pub fn iter(&self) -> Iter<'_, Entry> {
		self.0.iter()
	}
}

impl AsRef<Vec<Entry>> for EntryList {
	fn as_ref(&self) -> &Vec<Entry> {
		&self.0
	}
}

impl AsMut<Vec<Entry>> for EntryList {
	fn as_mut(&mut self) -> &mut Vec<Entry> {
		&mut self.0
	}
}

#[derive(Debug, Clone, Copy)]
pub struct Entry {
	pub kind: EntryKind,
	pub header_filled_bytes: usize,
	pub offset: usize,
	pub compressed_size: u64,
	pub decompressed_size: u64,
}

impl Entry {
	pub fn is_deltified(&self) -> bool {
		self.kind.is_delta_ref() || self.kind.is_delta_ofs()
	}
}

#[repr(u8)]
#[derive(Debug, Clone, Copy)]
pub enum EntryKind {
	Invalid = 0,
	Commit = 1,
	Tree = 2,
	Blob = 3,
	Tag = 4,
	Reserved = 5,
	OfsDelta { offset: isize } = 6,
	RefDelta { oid: Oid } = 7,
}

impl TryFrom<EntryKind> for ObjectKind {
	type Error = Error;

	fn try_from(value: EntryKind) -> Result<Self> {
		match value {
			EntryKind::Commit => Ok(ObjectKind::Commit),
			EntryKind::Tree => Ok(ObjectKind::Tree),
			EntryKind::Blob => Ok(ObjectKind::Blob),
			EntryKind::Tag => Ok(ObjectKind::Tag),
			_ => Err(Error::UnrecognizeObjectKind),
		}
	}
}
impl From<super::EntryKind> for EntryKind {
	fn from(value: super::EntryKind) -> Self {
		match value {
			super::EntryKind::Commit => EntryKind::Commit,
			super::EntryKind::Tree => EntryKind::Tree,
			super::EntryKind::Blob => EntryKind::Blob,
			super::EntryKind::Tag => EntryKind::Tag,
			super::EntryKind::Invalid => EntryKind::Invalid,
			super::EntryKind::Reserved => EntryKind::Reserved,
			super::EntryKind::OfsDelta => EntryKind::OfsDelta { offset: 0 },
			super::EntryKind::RefDelta => EntryKind::RefDelta {
				oid: Oid::zero_id_sha1(),
			},
		}
	}
}

impl EntryKind {
	pub fn is_delta_ref(&self) -> bool {
		matches!(self, EntryKind::RefDelta { .. })
	}

	pub fn is_delta_ofs(&self) -> bool {
		matches!(self, EntryKind::OfsDelta { .. })
	}

	pub fn delta_ref(&self) -> Option<Oid> {
		match self {
			Self::RefDelta { oid } => Some(*oid),
			_ => None,
		}
	}

	pub fn delta_ref_unchecked(&self) -> Oid {
		self.delta_ref().unwrap()
	}

	pub fn delta_ofs(&self) -> Option<isize> {
		match self {
			Self::OfsDelta { offset } => Some(*offset),
			_ => None,
		}
	}

	pub fn delta_ofs_unchecked(&self) -> isize {
		self.delta_ofs().unwrap()
	}

	pub const fn commit() -> Self {
		Self::Commit
	}

	pub const fn tree() -> Self {
		Self::Tree
	}

	pub const fn blob() -> Self {
		Self::Blob
	}

	pub const fn tag() -> Self {
		Self::Tag
	}

	pub fn ofs_delta(offset: isize) -> Self {
		Self::OfsDelta { offset }
	}

	pub fn ref_delta(oid: Oid) -> Self {
		Self::RefDelta { oid }
	}
}
