/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{error, result::Result};

pub mod metadata;

pub const PACK_SIGNATURE: [u8; 4] = [b'P', b'A', b'C', b'K'];

//  __________________________________
// |  ______________________________  |
// | |PACK HEADER                   | |
// | |______________________________| |
// | |           |         |        | |
// | | Signature | version | length | |
// | |    PACK   |   u32   |   u32  | |
// | |___________|_________|________| |
// |  ______________________________  |
// | |PACK PAYLOAD (ENTRY LIST)     | |
// | |______________________________| |
// | |  __________________________  | |
// | | |                          | | |
// | | |         ENTRY i          | | |
// | | |__________________________| | |
// | |  __________________________  | |
// | | |                          | | |
// | | |        ENTRY i+1         | | |
// | | |__________________________| | |
// | |              :               | |
// | |              :               | |
// | |              :               | |
// | |              :               | |
// | |  __________________________  | |
// | | |                          | | |
// | | |         ENTRY n          | | |
// | | |__________________________| | |
// | |______________________________| |
// |  ______________________________  |
// | |         SHA checksum         | |
// | |______________________________| |
// |__________________________________|
#[derive(Debug, Clone)]
pub struct Pack {
	header: Header,
	record: Record,
}

impl<'a> TryFrom<&'a [u8]> for Pack {
	type Error = error::Error;

	fn try_from(value: &'a [u8]) -> Result<Self> {
		let header = Header::try_from(&value[..12])?;

		Ok(Self {
			header,
			record: Record(Vec::new()),
		})
	}
}

#[derive(Debug, Clone, Copy)]
pub struct Header {
	pub signature: [u8; 4],
	pub version: u32,
	pub len: u32,
}

impl Header {
	pub fn is_valid_signature(&self) -> bool {
		self.signature == PACK_SIGNATURE
	}
}

impl<'a> TryFrom<&'a [u8]> for Header {
	type Error = error::Error;

	fn try_from(value: &'a [u8]) -> Result<Self> {
		let (signature, version, len) = (&value[..4], &value[4..8], &value[8..12]);

		if signature != PACK_SIGNATURE {
			return Err(error::Error::BadSignature);
		}

		let version = u32::from_be_bytes(version.try_into()?);
		let len = u32::from_be_bytes(len.try_into()?);

		Ok(Self {
			signature: signature.try_into()?,
			version,
			len,
		})
	}
}

#[derive(Debug, Clone)]
pub struct Record(Vec<Entry>);

#[derive(Debug, Clone)]
pub struct Entry {
	pub header: EntryHeader,
}

#[derive(Debug, Clone, Copy)]
pub struct EntryHeader {
	pub size: u128,
	pub kind: EntryKind,
	// TODO: remove this field and replace in for method to calculated from size and kind info
	filled_bytes: usize,
}

impl EntryHeader {
	pub fn filled_bytes(&self) -> usize {
		self.filled_bytes
	}

	pub fn is_deltified(&self) -> bool {
		matches!(self.kind, EntryKind::OfsDelta) || matches!(self.kind, EntryKind::RefDelta)
	}
}

impl From<&[u8]> for EntryHeader {
	fn from(value: &[u8]) -> Self {
		let mut idx = 0;

		loop {
			if value[idx] < 0x80 {
				break;
			}
			idx += 1;
		}
		idx += 1;

		let header = &value[..idx];

		let kind = (header[0] >> 4) & 0x7;
		let kind = EntryKind::from(kind);

		let mut size = header[0] as u128 & 0xf;
		let mut shift = 4u128;

		for b in header[1..].iter() {
			size |= (*b as u128 & 0x7f) << shift;
			shift += 7;
		}

		EntryHeader {
			size,
			kind,
			filled_bytes: idx,
		}
	}
}

#[derive(Debug, Clone, Copy)]
pub enum EntryKind {
	Invalid = 0,
	Commit = 1,
	Tree = 2,
	Blob = 3,
	Tag = 4,
	Reserved = 5,
	OfsDelta = 6,
	RefDelta = 7,
}

impl From<u8> for EntryKind {
	fn from(value: u8) -> Self {
		match value {
			1 => Self::Commit,
			2 => Self::Tree,
			3 => Self::Blob,
			4 => Self::Tag,
			5 => Self::Reserved,
			6 => Self::OfsDelta,
			7 => Self::RefDelta,
			_ => Self::Invalid,
		}
	}
}
