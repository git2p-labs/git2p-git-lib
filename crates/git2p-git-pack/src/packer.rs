/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::result::Result;
use git2p_git_odb::odb::GitOdb;

pub struct Packer<O: GitOdb> {
	odb: O,
}

impl<O: GitOdb> Packer<O> {
	pub fn new(odb: O) -> Self {
		Self { odb }
	}

	pub fn add() -> Result<()> {
		Ok(())
	}
}
