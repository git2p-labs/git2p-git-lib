/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use git2p_git_object::kind::ObjectKind;
use git2p_git_oid::oid::Oid;
use std::{error, fmt::Display};

#[derive(Debug)]
pub enum Error {
	BadObject {
		msg: String,
	},

	Object(git2p_git_object::error::Error),

	// this error occurs when an object is not in the object database
	ObjectDoesNotExist {
		oid: Oid,
	},

	BlobNotFound,

	TreeNotFound,

	InvalidTarget,

	Reference(git2p_git_references::error::Error),

	// this error occurs in ['OdbObject'] at 'try_into' methods, when the expected object is not
	// the same as input object
	IncompatibleConversion {
		input: ObjectKind,
		target: ObjectKind,
	},

	IO,

	Other,

	Blob(git2p_git_blob::error::Error),

	Commit(git2p_git_commit::error::Error),

	Tree(git2p_git_tree::error::Error),
}

impl Display for Error {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{self:?}")
	}
}

impl error::Error for Error {}

impl From<git2p_git_object::error::Error> for Error {
	fn from(value: git2p_git_object::error::Error) -> Self {
		Error::Object(value)
	}
}

impl From<git2p_git_references::error::Error> for Error {
	fn from(error: git2p_git_references::error::Error) -> Self {
		Self::Reference(error)
	}
}

impl From<git2p_git_helpers::convert::Error> for Error {
	fn from(_: git2p_git_helpers::convert::Error) -> Self {
		Error::BadObject {
			msg: String::from("non valid odb"),
		}
	}
}

impl From<git2p_git_blob::error::Error> for Error {
	fn from(error: git2p_git_blob::error::Error) -> Self {
		Self::Blob(error)
	}
}

impl From<git2p_git_commit::error::Error> for Error {
	fn from(error: git2p_git_commit::error::Error) -> Self {
		Self::Commit(error)
	}
}

impl From<git2p_git_tree::error::Error> for Error {
	fn from(error: git2p_git_tree::error::Error) -> Self {
		Self::Tree(error)
	}
}
