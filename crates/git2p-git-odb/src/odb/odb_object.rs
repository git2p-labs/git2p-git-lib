/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{error::Error, result::Result};
use git2p_git_blob::blob::Blob;
use git2p_git_commit::commit::Commit;
use git2p_git_object::{
	kind::ObjectKind,
	object::{Header, Object},
};
use git2p_git_oid::oid::Oid;
use git2p_git_serializers::ser::Serialize;
use git2p_git_tree::tree::Tree;

pub trait GitOdbObject {
	fn kind(&self) -> ObjectKind;

	fn size(&self) -> usize;

	/// returns the inner object serialized
	fn data(&self) -> Vec<u8>;

	/// calculates oid for this object
	fn id(&self) -> Oid;
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum OdbObject {
	Blob(Blob),
	Commit(Commit),
	Tree(Tree),
}

impl GitOdbObject for OdbObject {
	fn kind(&self) -> ObjectKind {
		self.header().kind()
	}

	fn size(&self) -> usize {
		self.header().size()
	}

	fn data(&self) -> Vec<u8> {
		match self {
			Self::Blob(b) => b.serialize(),
			Self::Tree(t) => t.serialize(),
			Self::Commit(c) => c.serialize(),
		}
	}

	fn id(&self) -> Oid {
		match self {
			Self::Blob(b) => b.oid().unwrap(),
			Self::Tree(t) => t.oid().unwrap(),
			Self::Commit(c) => c.oid().unwrap(),
		}
	}
}

impl OdbObject {
	pub fn serialize(&self) -> Result<Vec<u8>> {
		let serialized = match self.kind() {
			ObjectKind::Blob => self.clone().try_into_blob()?.serialized(),
			ObjectKind::Commit => self.clone().try_into_commit()?.serialized(),
			ObjectKind::Tree => self.clone().try_into_tree()?.serialized(),
			_ => unimplemented!(),
		};

		Ok(serialized)
	}

	pub fn as_blob(&self) -> Option<&Blob> {
		match self {
			Self::Blob(b) => Some(b),
			_ => None,
		}
	}

	pub fn as_commit(&self) -> Option<&Commit> {
		match self {
			Self::Commit(c) => Some(c),
			_ => None,
		}
	}

	pub fn as_tree(&self) -> Option<&Tree> {
		match self {
			Self::Tree(t) => Some(t),
			_ => None,
		}
	}

	pub fn try_into_blob(self) -> Result<Blob> {
		match self {
			Self::Blob(b) => Ok(b),
			_ => Err(Error::IncompatibleConversion {
				input: self.kind(),
				target: ObjectKind::Blob,
			}),
		}
	}

	pub fn try_into_commit(self) -> Result<Commit> {
		match self {
			Self::Commit(c) => Ok(c),
			_ => Err(Error::IncompatibleConversion {
				input: self.kind(),
				target: ObjectKind::Commit,
			}),
		}
	}

	pub fn try_into_tree(self) -> Result<Tree> {
		match self {
			Self::Tree(t) => Ok(t),
			_ => Err(Error::IncompatibleConversion {
				input: self.kind(),
				target: ObjectKind::Tree,
			}),
		}
	}

	pub fn header(&self) -> Header {
		match self {
			Self::Blob(b) => b.header(),
			Self::Tree(t) => t.header(),
			Self::Commit(c) => c.header(),
		}
	}

	pub fn try_from_packed(obj: &[u8], kind: ObjectKind) -> Result<Self> {
		match kind {
			ObjectKind::Blob => Self::try_from_blob_packed(obj),
			ObjectKind::Commit => Self::try_from_commit_packed(obj),
			ObjectKind::Tree => Self::try_from_tree_packed(obj),
			_ => todo!(),
		}
	}

	pub fn try_from_blob_packed(obj: &[u8]) -> Result<Self> {
		let blob = Blob::from_raw(obj)?;
		Ok(Self::Blob(blob))
	}

	pub fn try_from_commit_packed(obj: &[u8]) -> Result<Self> {
		let commit = Commit::from_raw(obj)?;
		Ok(Self::Commit(commit))
	}

	pub fn try_from_tree_packed(obj: &[u8]) -> Result<Self> {
		log::debug!("ODB OBJECT TRY TREE FROM PACKED");
		let tree = Tree::from_raw(obj)?;
		log::debug!("{:?}", tree);
		Ok(Self::Tree(tree))
	}
}
