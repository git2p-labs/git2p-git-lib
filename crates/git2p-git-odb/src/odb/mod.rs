/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{
	compressor::{Compress, Decompress},
	error::Error,
	result::Result,
};
use git2p_git_blob::blob::Blob;
use git2p_git_commit::commit::Commit;
use git2p_git_helpers::{ascii::NULL, convert::SplitBy};
use git2p_git_object::{
	kind::ObjectKind,
	object::{Header, Object},
};
use git2p_git_oid::oid::Oid;
use git2p_git_references::reference::{
	refname::{self, RefName},
	GitReference, Reference,
};
use git2p_git_serializers::de::TryDeserialize;
use git2p_git_tree::tree::Tree;
use std::{
	fs::{self, File},
	io::Write,
	path::{Path, PathBuf},
};

mod iter;
pub use iter::OdbIterator;

pub mod odb_object;
pub use odb_object::{GitOdbObject, OdbObject};

// NOTE: every ['Odb'] are associated with their own rapository
pub trait GitOdb: Send + Sync {
	type OdbIterator;
	type OdbObject: GitOdbObject;

	// NOTE: this should return the iter perse instead the wrapper
	// improve this...
	//
	/// A way to iterate over all database objects
	fn iter(&self) -> Self::OdbIterator;

	fn read(&self, oid: &Oid) -> Result<Self::OdbObject>;

	fn read_header(&self, oid: &Oid) -> Result<Header>;

	fn write(&self, object: Self::OdbObject) -> Result<Oid>;

	fn exists(&self, oid: &Oid) -> bool;
}

#[derive(Debug)]
pub struct Odb {
	db_path: PathBuf,
}

impl Odb {
	pub fn find_blob<P: AsRef<Path>>(&self, commit_id: &Oid, path: P) -> Result<Blob> {
		let path = path.as_ref();

		let Some(blob) = path.file_name() else {
			return Err(Error::InvalidTarget);
		};

		let Some(blob) = blob.to_str() else {
			return Err(Error::InvalidTarget);
		};

		let tree = if let Some(path) = path.parent() {
			if let Some(path) = path.to_str() {
				if path.is_empty() {
					self.root_tree_at_ref(refname::head())?
				} else {
					self.find_tree(commit_id, path)?
				}
			} else {
				self.root_tree_at_ref(refname::head())?
			}
		} else {
			self.root_tree_at_ref(refname::head())?
		};

		let entries = tree.entries();

		let Some(entry) = entries.iter().find(|e| e.name().eq(blob)) else {
			return Err(Error::BlobNotFound);
		};

		if entry.is_tree() {
			return Err(Error::InvalidTarget);
		}

		let blob = self.read(&entry.oid())?.try_into_blob()?;

		Ok(blob)
	}

	pub fn find_tree<P: AsRef<Path>>(&self, commit_id: &Oid, tree: P) -> Result<Tree> {
		let mut path = tree.as_ref().to_owned();

		while path.starts_with("/") {
			path = path.strip_prefix("/").unwrap().to_owned();
		}

		let Some(p) = path.to_str() else {
			return Err(Error::InvalidTarget);
		};

		if p.is_empty() {
			return Ok(self.root_tree_at_commit(commit_id)?);
		}

		let comps = path.components().collect::<Vec<_>>();

		let mut root_tree = self.root_tree_at_commit(&commit_id)?;

		let root_tree_oid = root_tree.oid()?;

		// let mut root_tree = root_tree.inner();

		for target in comps {
			let Some(name) = target.as_os_str().to_str() else {
				return Err(Error::InvalidTarget);
			};

			let entries = root_tree.entries();

			let Some(entry) = entries.iter().find(|e| e.name().eq(&name)) else {
				return Err(Error::TreeNotFound);
			};

			let Ok(tree) = self.read(&entry.oid())?.try_into_tree() else {
				return Err(Error::InvalidTarget);
			};

			root_tree = tree;
		}

		if root_tree.oid()?.eq(&root_tree_oid) {
			return Err(Error::TreeNotFound);
		}

		Ok(root_tree)
	}

	// it fails if reference not resolves to commit
	pub fn root_tree_at_ref(&self, refname: RefName) -> Result<Tree> {
		let reference = Reference::find(self.git_dir(), refname)?;

		let oid = reference.resolve()?.target()?;

		Ok(self.root_tree_at_commit(&oid)?)
	}

	// it fails if oid is not a commit or commit doesn't exist
	pub fn root_tree_at_commit(&self, oid: &Oid) -> Result<Tree> {
		let commit = self.read(&oid)?.try_into_commit()?;

		let tree = commit.tree();

		Ok(self.read(&tree)?.try_into_tree()?)
	}

	pub fn open<P: AsRef<Path>>(repo_path: P, bare: bool) -> Result<Self> {
		let db_path = if bare {
			repo_path.as_ref().join("objects")
		} else {
			repo_path.as_ref().join(".git/objects")
		};

		Ok(Self { db_path })
	}

	fn object_path(&self, oid: &Oid) -> Result<PathBuf> {
		let path = self.object_path_unchecked(oid);

		if !path.exists() {
			return Err(Error::ObjectDoesNotExist { oid: *oid });
		}

		Ok(path)
	}

	fn object_path_unchecked(&self, oid: &Oid) -> PathBuf {
		let oid_str = oid.to_string();

		let path = PathBuf::from(&self.db_path)
			.join(&oid_str[..2])
			.join(&oid_str[2..]);

		path
	}

	fn git_dir(&self) -> PathBuf {
		self.db_path.parent().unwrap().to_path_buf()
	}
}

impl GitOdb for Odb {
	type OdbIterator = OdbIterator;
	type OdbObject = OdbObject;

	fn iter(&self) -> OdbIterator {
		OdbIterator::new(&self.db_path)
	}

	fn read(&self, oid: &Oid) -> Result<Self::OdbObject> {
		let path = self.object_path(oid)?;

		let Ok(content) = fs::read(path) else {
			return Err(Error::IO);
		};

		let serialized = content.as_slice().decompress()?;
		let serialized = serialized.as_slice();

		let (header, data) = &serialized.split_by(&NULL)?;

		let header = Header::try_deserialize(header)?;

		match header.kind() {
			ObjectKind::Blob => {
				let blob = Blob::from_raw(data)?;
				Ok(OdbObject::Blob(blob))
			}
			ObjectKind::Commit => {
				let commit = Commit::from_raw(data)?;
				Ok(OdbObject::Commit(commit))
			}
			ObjectKind::Tree => {
				let tree = Tree::from_raw(data)?;
				Ok(OdbObject::Tree(tree))
			}
			_ => todo!("tag and any objects are not yet implemented"),
		}
	}

	fn read_header(&self, oid: &Oid) -> Result<Header> {
		let path = self.object_path(oid)?;

		let Ok(content) = fs::read(path) else {
			return Err(Error::IO);
		};

		let header = content.as_slice().decompress_until(NULL)?;

		let header =
			Header::try_deserialize(&header.as_slice()).expect("error deserializing header");

		Ok(header)
	}

	fn write(&self, object: Self::OdbObject) -> Result<Oid> {
		let path = self.object_path_unchecked(&object.id());

		if path.exists() {
			return Ok(object.id());
		}

		fs::create_dir_all(path.parent().unwrap()).unwrap();

		let mut file_object = File::create(path).unwrap();
		file_object.write_all(&object.compress()?).unwrap();

		Ok(object.id())
	}

	fn exists(&self, oid: &Oid) -> bool {
		self.object_path(oid).is_ok()
	}
}
