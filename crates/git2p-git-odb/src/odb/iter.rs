/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use git2p_git_oid::oid::Oid;
use std::{
	fs::{self, DirEntry},
	ops::Deref,
	path::PathBuf,
};

//PERFORMANCE:
//  this generates a bottle neck when an [`Odb`] tries getting an [`OdbIterator`], this should be
//  an asynchronous opperation, this should pinning, or sharing an Arc, one thread should be
//  responsible for populating the iterator while leaves the iterator caller free for use
//
/// a way to iter over all database's objects
#[derive(Debug)]
pub struct OdbIterator {
	vec: Vec<Oid>,
}

impl OdbIterator {
	pub(super) fn new(path: &PathBuf) -> Self {
		let read_dir = fs::read_dir(path).unwrap();

		let vec = read_dir
			.flat_map(|entry| objects(entry.unwrap()))
			.collect::<Vec<_>>();

		Self { vec }
	}
}

impl Deref for OdbIterator {
	type Target = Vec<Oid>;

	fn deref(&self) -> &Self::Target {
		&self.vec
	}
}

fn objects(dir: DirEntry) -> Vec<Oid> {
	let path = dir.path();
	let base_name = path.file_name().unwrap().to_str().unwrap();

	let read_dir = fs::read_dir(&path).unwrap();

	read_dir
		.map(|e| {
			let suffix = e.unwrap().file_name().into_string().unwrap();
			let oid_str = format!("{}{}", base_name, suffix);

			Oid::from_str_arr(oid_str.as_bytes()).unwrap()
		})
		.collect::<Vec<_>>()
}
