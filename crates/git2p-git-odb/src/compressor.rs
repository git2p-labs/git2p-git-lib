/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{
	error::Error,
	odb::{GitOdbObject, OdbObject},
	result::Result,
};
use flate2::{read::ZlibDecoder, write::ZlibEncoder, Compression};
use std::io::prelude::*;

pub trait Compress: GitOdbObject {
	fn compress(&self) -> Result<Vec<u8>>;
}

impl Compress for OdbObject {
	fn compress(&self) -> Result<Vec<u8>> {
		let mut e = ZlibEncoder::new(Vec::new(), Compression::default());

		if e.write_all(&self.serialize()?[..]).is_ok() {
			if let Ok(compressed) = e.finish() {
				return Ok(compressed);
			}
		}

		Err(Error::IO)
	}
}

pub trait Decompress {
	fn decompress(&self) -> Result<Vec<u8>>;

	fn decompress_until(&self, until: u8) -> Result<Vec<u8>>;
}

impl Decompress for &[u8] {
	fn decompress(&self) -> Result<Vec<u8>> {
		let mut de = ZlibDecoder::new(*self);
		let mut buf = Vec::new();

		if de.read_to_end(&mut buf).is_ok() {
			Ok(buf)
		} else {
			Err(Error::IO)
		}
	}

	fn decompress_until(&self, until: u8) -> Result<Vec<u8>> {
		let de = ZlibDecoder::new(*self);
		let mut buf = Vec::new();

		for b in de.bytes() {
			match b {
				Ok(b) => {
					if b == until {
						break;
					}
					buf.push(b);
				}
				Err(_) => return Err(Error::IO),
			}
		}
		Ok(buf)
	}
}
