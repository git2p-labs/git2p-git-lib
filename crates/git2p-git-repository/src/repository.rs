/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::result::Result;
use git2p_git_oid::oid::Oid;
use git2p_git_references::reference::{
	refname::{NameFormat, RefName},
	GitReference, Kind, Reference,
};
use std::path::{Path, PathBuf};

mod init;
pub use init::Init;

mod odb;
pub use odb::Odb;

mod open;
pub use open::Open;

mod references;
pub use references::References;

mod state;
pub use state::State;

pub trait GitRepository<P>
where
	P: AsRef<Path>,
{
	type Repository;

	fn create_ref(
		&self,
		refname: &str,
		format: NameFormat,
		target: &str,
		kind: Kind,
	) -> Result<Reference>;

	fn delete_ref(&self, refname: &str, format: NameFormat) -> Result<()>;

	fn update_direct_ref(
		&self,
		refname: &str,
		format: NameFormat,
		target: &Oid,
	) -> Result<Reference>;

	fn update_symbolic_ref(
		&self,
		refname: &str,
		format: NameFormat,
		target: &str,
	) -> Result<Reference>;

	fn is_empty(&self) -> Result<bool>;

	fn head(&self) -> Result<git2p_git_references::reference::Reference>;

	fn references(&self) -> Result<Vec<Reference>>;
}

#[derive(Debug)]
pub struct Repository {
	bare: bool,
	path: PathBuf,
}

impl Repository {
	pub fn git_dir(&self) -> PathBuf {
		if self.bare {
			self.path.clone()
		} else {
			self.path.join(".git")
		}
	}
}

impl GitRepository<PathBuf> for Repository {
	type Repository = Self;

	fn create_ref(
		&self,
		refname: &str,
		format: NameFormat,
		target: &str,
		kind: Kind,
	) -> Result<Reference> {
		let refname = RefName::new(refname, format)?;

		<Repository as References<PathBuf>>::create_ref(self, refname, target, kind)
	}

	fn delete_ref(&self, refname: &str, format: NameFormat) -> Result<()> {
		<Repository as References<PathBuf>>::delete_ref(self, refname, format)
	}

	fn update_direct_ref(
		&self,
		refname: &str,
		format: NameFormat,
		target: &Oid,
	) -> Result<Reference> {
		<Repository as References<PathBuf>>::update_direct_ref(self, refname, format, target)
	}

	fn update_symbolic_ref(
		&self,
		refname: &str,
		format: NameFormat,
		target: &str,
	) -> Result<Reference> {
		<Repository as References<PathBuf>>::update_symbolic_ref(self, refname, format, target)
	}

	fn is_empty(&self) -> Result<bool> {
		if !self.head()?.is_unborn()? {
			return Ok(false);
		}

		for reference in self.references()? {
			if reference.is_tag() || reference.is_branch() {
				if !reference.is_unborn()? {
					return Ok(false);
				}
			}
		}

		Ok(true)
	}

	fn head(&self) -> Result<git2p_git_references::reference::Reference> {
		<Repository as References<PathBuf>>::head(self)
	}

	fn references(&self) -> Result<Vec<Reference>> {
		<Repository as References<PathBuf>>::references(self)
	}
}
