/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{
	repository::{GitRepository, Open, Repository},
	result::Result,
};
use std::{
	fs, io,
	path::{Path, PathBuf},
};

pub trait Init<P>: GitRepository<P>
where
	P: AsRef<Path>,
{
	// type InitOpts;

	// fn init_opts(path: &P, opts: Self::InitOpts) -> Result<Self::Repository>;

	// fn init(path: &P) -> Result<Self::Repository>;

	fn init_bare(path: &P) -> Result<Self::Repository>;
}

impl Init<PathBuf> for Repository {
	// type InitOpts = InitOpts;

	// fn init_opts(path: &PathBuf, opts: Self::InitOpts) -> Result<Self::Repository> {
	// 	todo!()
	// }

	// fn init(path: &PathBuf) -> Result<Self::Repository> {
	// 	todo!()
	// }

	fn init_bare(path: &PathBuf) -> Result<Self::Repository> {
		let template_path = PathBuf::from(env!("CARGO_MANIFEST_DIR")).join("template");

		copy_dir_all(template_path, path)?;

		<Repository as Open<PathBuf>>::open_bare(path)
	}
}

fn copy_dir_all<S: AsRef<Path>, D: AsRef<Path>>(src: S, dst: D) -> io::Result<()> {
	fs::create_dir_all(&dst)?;
	for entry in fs::read_dir(src)? {
		let entry = entry?;
		let ty = entry.file_type()?;
		if ty.is_dir() {
			copy_dir_all(entry.path(), dst.as_ref().join(entry.file_name()))?;
		} else {
			fs::copy(entry.path(), dst.as_ref().join(entry.file_name()))?;
		}
	}
	Ok(())
}

/* #[derive(Debug)]
pub struct InitOpts<'repo, P>
where
	P: AsRef<Path> + 'repo,
{
	pub bare: bool,
	pub mode: InitMode,
	pub workdir_path: Option<P>,
	pub description: Option<&'repo str>,
	pub template_path: Option<P>,
	pub initial_head: Option<&'repo str>,
	pub origin_url: Option<&'repo str>,
}

#[derive(Debug, Default)]
pub enum InitMode {
	// 777
	SharedAll,
	// 770
	SharedGroup,
	#[default]
	SharedUmask,
} */
