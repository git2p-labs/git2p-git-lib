/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::repository::{GitRepository, Repository};
use std::path::{Path, PathBuf};

pub trait State<P>: GitRepository<P>
where
	P: AsRef<Path>,
{
	fn is_bare(&self) -> bool;

	fn is_empty(&self) -> bool;
}

impl State<PathBuf> for Repository {
	fn is_bare(&self) -> bool {
		todo!()
	}

	fn is_empty(&self) -> bool {
		todo!()
	}
}
