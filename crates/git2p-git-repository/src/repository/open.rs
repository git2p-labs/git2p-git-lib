/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{
	error::{Error, OpenError},
	repository::{GitRepository, Repository},
	result::Result,
};
use std::path::{Path, PathBuf};

pub trait Open<P>: GitRepository<P>
where
	P: AsRef<Path>,
{
	fn open(path: &P) -> Result<Self::Repository>;

	fn open_bare(path: &P) -> Result<Self::Repository>;
}

impl Open<PathBuf> for Repository {
	fn open(path: &PathBuf) -> Result<Self> {
		if path.canonicalize().is_err() {
			return Err(Error::Open(OpenError::DoesNotExists));
		}

		if path.join(".git").canonicalize().is_err() {
			return Err(Error::Open(OpenError::IsNotValidRepository));
		}

		Ok(Self {
			bare: false,
			path: path.to_owned(),
		})
	}

	fn open_bare(path: &PathBuf) -> Result<Self> {
		if path.canonicalize().is_err() {
			return Err(Error::Open(OpenError::DoesNotExists));
		}

		Ok(Self {
			bare: true,
			path: path.to_owned(),
		})
	}
}

// /git2p-labs/gitcrab/-/tree/master/gitcrab-web/scss?ref_type=heads
//| namespace | name | - | tree/master | $path |
