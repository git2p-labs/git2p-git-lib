/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{
	error::Error,
	repository::{GitRepository, Repository},
	result::Result,
};
use git2p_git_oid::oid::Oid;
use git2p_git_references::{
	error,
	reference::{
		refname::{self, NameFormat, RefName},
		GitReference, Kind, Reference,
	},
};
use std::path::{Path, PathBuf};

pub trait References<P>: GitRepository<P>
where
	P: AsRef<Path>,
{
	type Reference: GitReference;

	fn create_ref(
		repo: &Self::Repository,
		refname: RefName,
		target: &str,
		kind: Kind,
	) -> Result<Self::Reference>;

	fn delete_ref(repo: &Self::Repository, refname: &str, format: NameFormat) -> Result<()>;

	fn update_direct_ref(
		repo: &Self::Repository,
		refname: &str,
		format: NameFormat,
		target: &Oid,
	) -> Result<Reference>;

	fn update_symbolic_ref(
		repo: &Self::Repository,
		refname: &str,
		format: NameFormat,
		target: &str,
	) -> Result<Reference>;

	fn head(repo: &Self::Repository) -> Result<Self::Reference>;

	fn references(repo: &Self::Repository) -> Result<Vec<Reference>>;

	fn heads(repo: &Self::Repository) -> Result<Vec<Reference>>;

	fn tags(repo: &Self::Repository) -> Result<Vec<Reference>>;

	fn remotes(repo: &Self::Repository) -> Result<Vec<Reference>>;
}

impl References<PathBuf> for Repository {
	type Reference = Reference;

	fn create_ref(
		repo: &Self::Repository,
		refname: RefName,
		target: &str,
		kind: Kind,
	) -> Result<Self::Reference> {
		let git_dir = repo.git_dir();

		let reference = match kind {
			Kind::Direct => {
				let Ok(target) = Oid::try_from(target.to_owned()) else {
					return Err(Error::References(error::Error::InvalidOid));
				};

				Reference::create_direct(git_dir, refname, target)
			}
			Kind::Symbolic => Reference::create_symbolic(git_dir, refname, target),
		}?;

		Ok(reference)
	}

	fn delete_ref(repo: &Self::Repository, refname: &str, format: NameFormat) -> Result<()> {
		let refname = RefName::new(refname, format)?;

		let reference = Reference::find(repo.git_dir(), refname)?;

		Ok(reference.delete()?)
	}

	fn update_direct_ref(
		repo: &Self::Repository,
		refname: &str,
		format: NameFormat,
		target: &Oid,
	) -> Result<Reference> {
		let refname = RefName::new(refname, format)?;

		let reference = Reference::find(repo.git_dir(), refname)?;

		Ok(reference.update_target(target)?)
	}

	fn update_symbolic_ref(
		repo: &Self::Repository,
		refname: &str,
		format: NameFormat,
		target: &str,
	) -> Result<Reference> {
		let refname = RefName::new(refname, format)?;

		let reference = Reference::find(repo.git_dir(), refname)?;

		Ok(reference.update_symbolic_target(target)?)
	}

	fn head(repo: &Self::Repository) -> Result<Self::Reference> {
		Ok(Reference::find(repo.git_dir(), refname::head())?)
	}

	fn references(repo: &Self::Repository) -> Result<Vec<Reference>> {
		Ok(git2p_git_references::all(repo.git_dir())?)
	}

	fn heads(repo: &Self::Repository) -> Result<Vec<Reference>> {
		Ok(git2p_git_references::heads(repo.git_dir())?)
	}

	fn tags(repo: &Self::Repository) -> Result<Vec<Reference>> {
		Ok(git2p_git_references::tags(repo.git_dir())?)
	}

	fn remotes(repo: &Self::Repository) -> Result<Vec<Reference>> {
		Ok(git2p_git_references::remotes(repo.git_dir())?)
	}
}
