/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{error::Error, result::Result};
use git2p_git_helpers::ascii::NULL;
use git2p_git_object::{
	kind::ObjectKind,
	object::{Header, Object},
};
use git2p_git_serializers::{de::TryDeserialize, ser::Serialize};
use serde::{Deserialize, Serialize as Ser};

#[derive(Debug, Clone, PartialEq, Eq, Ser, Deserialize)]
pub struct Blob {
	content: Vec<u8>,
}

impl Object<'_, Error> for Blob {
	fn header(&self) -> Header {
		Header::new(ObjectKind::Blob, self.content.len())
	}

	fn serialized(&self) -> Vec<u8> {
		[
			self.header().serialize().as_slice(),
			&[NULL],
			Serialize::serialize(self).as_slice(),
		]
		.concat()
	}

	fn from_raw(raw: &[u8]) -> Result<Self> {
		Self::try_deserialize(&raw)
	}
}

impl Blob {
	pub fn content(&self) -> &[u8] {
		&self.content
	}

	pub fn content_as_string(&self) -> Result<String> {
		match String::from_utf8(self.content.to_vec()) {
			Ok(s) => Ok(s),
			Err(_) => Err(Error::NonUtf8),
		}
	}
}

git2p_git_serializers::serialize_impl!(Blob, Vec<u8>, 'ser);

impl From<&Blob> for Vec<u8> {
	fn from(value: &Blob) -> Self {
		value.content.clone()
	}
}

impl From<Blob> for Vec<u8> {
	fn from(value: Blob) -> Self {
		Self::from(&value)
	}
}

git2p_git_serializers::deserialize_impl!(Blob, &'de [u8], Error, 'de);

impl TryFrom<&&[u8]> for Blob {
	type Error = Error;

	fn try_from(value: &&[u8]) -> Result<Self> {
		Ok(Self {
			content: value.to_vec(),
		})
	}
}
