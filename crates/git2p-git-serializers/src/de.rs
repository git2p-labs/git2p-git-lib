/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

pub trait Deserialize {
	fn deserialize(_: &[u8]) -> Self;
}

pub trait TryDeserialize<'de, T: 'de, E>: TryFrom<&'de T, Error = E> {
	fn try_deserialize(ser: &'de T) -> Result<Self, Self::Error>;
}

#[macro_export]
macro_rules! deserialize_impl {
	($structname: ty, $from: ty, $err: ty, $life: lifetime) => {
		impl<$life> TryDeserialize<$life, $from, $err> for $structname {
			fn try_deserialize(ser: &'de $from) -> std::result::Result<$structname, $err> {
				<$structname>::try_from(ser)
			}
		}
	};
}
