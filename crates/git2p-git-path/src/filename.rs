/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{error::Error, result::Result};
use std::path::Path;

pub trait FileName: AsRef<Path> {
	fn file_name(&self) -> Result<String>;
}

#[cfg(target_family = "unix")]
impl<P: AsRef<Path>> FileName for P {
	fn file_name(&self) -> Result<String> {
		let path = self.as_ref();
		if let Some(name) = path.file_name() {
			Ok(name.to_str().unwrap().to_owned())
		} else {
			Err(Error::Inconsistent)
		}
	}
}

#[cfg(all(target_family = "windows", target_family = "wasm"))]
impl<P: AsRef<Path>> FileName for P {
	fn file_name(&self) -> Result<String> {
		unimplemented!()
	}
}
