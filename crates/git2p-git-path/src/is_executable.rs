/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#[cfg(target_family = "unix")]
use std::{os::unix::fs::PermissionsExt, path::Path};

#[cfg(not(target_arch = "wasm32"))]
pub trait IsExecutable {
	fn is_executable(&self) -> bool;
}

#[cfg(target_family = "unix")]
impl<P: AsRef<Path>> IsExecutable for P {
	fn is_executable(&self) -> bool {
		if let Ok(m) = self.as_ref().metadata() {
			m.is_file() && m.permissions().mode() & 0o111 != 0
		} else {
			false
		}
	}
}

/*#[cfg(all(target_family = "windows", target_family = "wasm"))]
impl<P: AsRef<Path>> IsExecutable for P {
	fn is_executable(&self) -> bool {
		unimplemented!()
	}
}*/
