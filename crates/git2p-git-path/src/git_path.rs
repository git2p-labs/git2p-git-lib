/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{error::Error, relative_to::RelativeTo, result::Result};
use serde::{Deserialize, Serialize};
use std::{
	fmt::Display,
	fs,
	path::{Path, PathBuf},
};

// /root/repo/path/my/folder/my/file.txt
// root = /root/repo/path
// path = /my/folder/file.txt
//
// /root/repo/path/my/folder
// root = /root/repo/path
// path = /my/folder
#[derive(Debug, Default, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct GitPath {
	// path of repository
	root: PathBuf,

	// path of this
	path: PathBuf,
}

impl Display for GitPath {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{}", self.absolute_path().to_str().unwrap())
	}
}

impl Ord for GitPath {
	fn cmp(&self, other: &Self) -> std::cmp::Ordering {
		let path = self.absolute_path();

		let mut path_str = self.to_string();

		if path.is_dir() {
			path_str.push('/');
		};

		let other_path = other.to_string();

		path_str.cmp(&other_path)
	}
}

impl PartialOrd for GitPath {
	fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
		Some(self.cmp(other))
	}
}

impl GitPath {
	pub fn absolute_path(&self) -> PathBuf {
		self.path.to_owned()
	}

	#[cfg(not(target_family = "wasm"))]
	pub fn relative_path(&self) -> PathBuf {
		self.absolute_path().relative_to(&self.root).unwrap()
	}
}

#[cfg(not(target_family = "wasm"))]
impl TryFrom<&PathBuf> for GitPath {
	type Error = Error;

	fn try_from(value: &PathBuf) -> Result<Self> {
		let root = find_root(&value)?;

		let path = if let Ok(p) = value.canonicalize() {
			p
		} else {
			return Err(Error::DoesNotExist);
		};

		if path.relative_to(&root).is_err() {
			return Err(Error::IsNotAGitPath);
		};

		Ok(Self { root, path })
	}
}

fn find_root<P: AsRef<Path>>(from: P) -> Result<PathBuf> {
	if let Ok(c) = fs::canonicalize(from) {
		for p in c.ancestors() {
			if p.join(".git").exists() {
				return Ok(p.to_path_buf());
			}
		}
		Err(Error::IsNotAGitPath)
	} else {
		Err(Error::DoesNotExist)
	}
}
