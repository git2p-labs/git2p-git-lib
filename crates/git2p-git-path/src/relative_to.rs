/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{error::Error, result::Result};
use std::path::{Path, PathBuf};

pub trait RelativeTo {
	fn relative_to(&self, to: &Self) -> Result<PathBuf>;
}

#[cfg(not(target_family = "wasm"))]
impl<P: AsRef<Path>> RelativeTo for P {
	fn relative_to(&self, to: &Self) -> Result<PathBuf> {
		let base = self.as_ref().canonicalize();
		let to = to.as_ref().canonicalize();

		if let (Ok(base), Ok(to)) = (base, to) {
			if let Ok(rel) = base.strip_prefix(to) {
				Ok(PathBuf::from(rel))
			} else {
				Err(Error::Inconsistent)
			}
		} else {
			Err(Error::Inconsistent)
		}
	}
}

#[cfg(all(target_family = "windows", target_family = "wasm"))]
impl<P: AsRef<Path>> RelativeTo for P {
	fn relative_to(&self, _: &Self) -> Result<PathBuf> {
		unimplemented!()
	}
}
