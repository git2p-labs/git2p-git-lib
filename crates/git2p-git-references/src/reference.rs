/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{error::Error, result::Result};
use git2p_git_oid::oid::Oid;
use serde::{Deserialize, Serialize};
use std::{
	fs::{self, OpenOptions},
	io::{Read, Write},
	path::{Path, PathBuf},
};

pub mod direct;
use direct::DirectRef;

pub mod refname;
use refname::{NameFormat, RefName};

pub mod symbolic;
use symbolic::SymbolicRef;

pub trait GitReference {
	type Reference;

	fn kind(&self) -> Kind;

	fn is_symbolic(&self) -> bool {
		matches!(self.kind(), Kind::Symbolic)
	}

	fn is_direct(&self) -> bool {
		!self.is_symbolic()
	}

	fn create_direct<P: AsRef<Path>>(
		git_dir: P,
		refname: RefName,
		target: Oid,
	) -> Result<Self::Reference>;

	fn create_symbolic<P: AsRef<Path>>(
		git_dir: P,
		refname: RefName,
		target: &str,
	) -> Result<Self::Reference>;

	fn delete(self) -> Result<()>;

	fn is_branch(&self) -> bool;

	fn is_remote(&self) -> bool;

	fn is_tag(&self) -> bool;

	fn is_unborn(&self) -> Result<bool>;

	fn name(&self) -> Result<RefName>;

	fn target(&self) -> Result<Oid>;

	fn symbolic_target(&self) -> Result<RefName>;

	fn resolve(&self) -> Result<Self::Reference>;

	fn rename(self, refname: RefName) -> Result<Self::Reference>;

	fn update_target(self, target: &Oid) -> Result<Self::Reference>;

	fn update_symbolic_target(self, target: &str) -> Result<Self::Reference>;

	fn find<P: AsRef<Path>>(git_dir: P, refname: RefName) -> Result<Self::Reference>;
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
pub enum Kind {
	Direct,
	Symbolic,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub enum Reference {
	Direct { reference: DirectRef },
	Symbolic { reference: SymbolicRef },
}

impl GitReference for Reference {
	type Reference = Self;

	fn kind(&self) -> Kind {
		match self {
			Reference::Direct { .. } => Kind::Direct,
			Reference::Symbolic { .. } => Kind::Symbolic,
		}
	}

	fn create_direct<P: AsRef<Path>>(
		git_dir: P,
		refname: RefName,
		target: Oid,
	) -> Result<Self::Reference> {
		if !git_dir.as_ref().canonicalize().is_ok() {
			return Err(Error::BadGitDir);
		}

		let path = git_dir.as_ref().join(refname.to_string());

		let prefix = path.parent().unwrap();

		fs::create_dir_all(prefix).unwrap();

		let mut file = OpenOptions::new()
			.create(true)
			.write(true)
			.truncate(true)
			.open(path)
			.unwrap();

		file.write_all(target.to_string().as_bytes())?;

		let reference = Self::Direct {
			reference: DirectRef::new(refname, &target, git_dir)?,
		};

		Ok(reference)
	}

	fn create_symbolic<P: AsRef<Path>>(
		git_dir: P,
		refname: RefName,
		target: &str,
	) -> Result<Self::Reference> {
		if !git_dir.as_ref().canonicalize().is_ok() {
			return Err(Error::BadGitDir);
		}

		let path = git_dir.as_ref().join(refname.to_string());

		let prefix = path.parent().unwrap();

		fs::create_dir_all(prefix).unwrap();

		let mut file = OpenOptions::new()
			.create(true)
			.write(true)
			.truncate(true)
			.open(path)
			.unwrap();

		file.write_all(target.as_bytes())?;

		let reference = Self::Symbolic {
			reference: SymbolicRef::new(
				refname,
				RefName::new(target, NameFormat::Normal)?,
				git_dir,
			)?,
		};

		Ok(reference)
	}

	fn delete(self) -> Result<()> {
		let name = self.name()?;

		if name.eq(&refname::head()) {
			return Err(Error::HeadCannotBeDeleted);
		}

		fs::remove_file(self.path()).unwrap();

		Ok(())
	}

	fn is_branch(&self) -> bool {
		self.path()
			.parent()
			.unwrap()
			.file_name()
			.unwrap()
			.eq("heads")
	}

	fn is_remote(&self) -> bool {
		self.path()
			.parent()
			.unwrap()
			.file_name()
			.unwrap()
			.eq("remotes")
	}

	fn is_tag(&self) -> bool {
		self.path()
			.parent()
			.unwrap()
			.file_name()
			.unwrap()
			.eq("tags")
	}

	fn is_unborn(&self) -> Result<bool> {
		match self.resolve() {
			Ok(r) => {
				if r.target()?.is_zero_id() {
					Ok(true)
				} else {
					Ok(false)
				}
			}
			Err(e) => match e {
				Error::ReferenceDoesNotExists => Ok(true),
				e => Err(e),
			},
		}
	}

	fn name(&self) -> Result<RefName> {
		match self {
			Reference::Direct { reference } => Ok(reference.refname()),
			Reference::Symbolic { reference } => Ok(reference.refname()),
		}
	}

	fn target(&self) -> Result<Oid> {
		match self {
			Reference::Direct { reference } => Ok(reference.target()),
			Reference::Symbolic { .. } => Err(Error::InvalidKind),
		}
	}

	fn symbolic_target(&self) -> Result<RefName> {
		match self {
			Reference::Direct { .. } => Err(Error::InvalidKind),
			Reference::Symbolic { reference } => Ok(reference.target()),
		}
	}

	fn resolve(&self) -> Result<Self::Reference> {
		match self {
			Reference::Direct { .. } => Ok(self.clone()),
			Reference::Symbolic { reference } => {
				let target = reference.target();
				let target = Self::find(self.git_dir(), target)?;

				target.resolve()
			}
		}
	}

	fn rename(self, refname: RefName) -> Result<Self::Reference> {
		if self.name()?.eq(&refname::head()) {
			return Err(Error::HeadCannotBeRenamed);
		}

		let mut contents = Vec::new();

		let mut file = OpenOptions::new()
			.create(false)
			.write(false)
			.read(true)
			.open(self.path())?;

		file.read_to_end(&mut contents)?;

		let new_ref = match self {
			Reference::Direct { .. } => {
				Self::create_direct(self.git_dir(), refname, Oid::from_str_arr(&contents[..40])?)
			}
			Reference::Symbolic { .. } => {
				let str = String::from_utf8(contents).unwrap();

				Self::create_symbolic(self.git_dir(), refname, &str)
			}
		}?;

		fs::remove_file(self.path())?;

		Ok(new_ref)
	}

	fn update_target(self, target: &Oid) -> Result<Self::Reference> {
		match self {
			Reference::Direct { reference } => Ok(Self::Direct {
				reference: reference.update_target(*target)?,
			}),
			Reference::Symbolic { .. } => return Err(Error::InvalidKind),
		}
	}

	fn update_symbolic_target(self, target: &str) -> Result<Self::Reference> {
		match self {
			Reference::Direct { .. } => return Err(Error::InvalidKind),
			Reference::Symbolic { reference } => Ok(Self::Symbolic {
				reference: reference.update_target(target)?,
			}),
		}
	}

	fn find<P: AsRef<Path>>(git_dir: P, refname: RefName) -> Result<Self::Reference> {
		if git_dir.as_ref().canonicalize().is_err() {
			return Err(Error::BadGitDir);
		}

		let path = git_dir.as_ref().join(refname.to_string());

		if !path.exists() {
			return Err(Error::ReferenceDoesNotExists);
		}

		if RefName::validate_name(&refname.to_string(), refname.format()).is_err() {
			return Err(Error::InvalidRefname);
		}

		let mut file = OpenOptions::new()
			.create(false)
			.write(false)
			.read(true)
			.open(path)?;

		let mut contents = Vec::new();

		file.read_to_end(&mut contents)?;

		if contents.len() < 40 {
			let target = String::from_utf8(contents).unwrap();

			let target = target.strip_prefix("ref:").unwrap().trim();

			let target = RefName::new(target, NameFormat::Normal)?;

			return Ok(Reference::Symbolic {
				reference: SymbolicRef::new(refname, target, git_dir)?,
			});
		}

		let Ok(oid) = Oid::from_str_arr(&contents[..40]) else {
			let target = String::from_utf8(contents).unwrap();

			let target = target.strip_prefix("ref:").unwrap().trim();

			let target = RefName::new(target, NameFormat::Normal)?;

			return Ok(Reference::Symbolic {
				reference: SymbolicRef::new(refname, target, git_dir)?,
			});
		};

		Ok(Reference::Direct {
			reference: DirectRef::new(refname, &oid, git_dir)?,
		})
	}
}

impl Reference {
	fn git_dir(&self) -> &PathBuf {
		match self {
			Reference::Direct { reference } => reference.git_dir(),
			Reference::Symbolic { reference } => reference.git_dir(),
		}
	}

	fn path(&self) -> PathBuf {
		match self {
			Reference::Direct { reference } => reference.path(),
			Reference::Symbolic { reference } => reference.path(),
		}
	}
}

impl<P: AsRef<Path>> TryFrom<crate::PathInfo<'_, P>> for Reference {
	type Error = Error;

	fn try_from(crate::PathInfo { git_dir, path }: crate::PathInfo<P>) -> Result<Self> {
		if !path.exists() {
			return Err(Error::ReferenceDoesNotExists);
		}

		let Ok(name) = path.strip_prefix(git_dir) else {
			return Err(Error::Other);
		};

		let Some(name) = name.to_str() else {
			return Err(Error::Other);
		};

		let refname = if name.starts_with("refs/") {
			RefName::new(name, NameFormat::Normal)?
		} else {
			match name {
				"HEAD" => refname::head(),
				_ => return Err(Error::InvalidRefname),
			}
		};

		let mut file = OpenOptions::new()
			.create(false)
			.write(false)
			.read(true)
			.open(path)?;

		let mut contents = Vec::new();

		file.read_to_end(&mut contents)?;

		if contents.len() < 40 {
			let target = String::from_utf8(contents).unwrap();

			let target = target.strip_prefix("ref:").unwrap().trim();

			let target = RefName::new(target, NameFormat::Normal)?;

			return Ok(Reference::Symbolic {
				reference: SymbolicRef::new(refname, target, git_dir)?,
			});
		}

		let Ok(oid) = Oid::from_str_arr(&contents[..40]) else {
			let target = String::from_utf8(contents).unwrap();

			let target = target.strip_prefix("ref:").unwrap().trim();

			let target = RefName::new(&target, NameFormat::Normal)?;

			return Ok(Reference::Symbolic {
				reference: SymbolicRef::new(refname, target, git_dir)?,
			});
		};

		Ok(Reference::Direct {
			reference: DirectRef::new(refname, &oid, git_dir)?,
		})
	}
}
