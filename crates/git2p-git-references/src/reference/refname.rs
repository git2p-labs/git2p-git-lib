/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::result::Result;
use serde::{Deserialize, Serialize};
use std::fmt::Display;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
pub enum NameFormat {
	Normal,
	OneLevel,
	RefspecPattern,
	RefspecShorthand,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct RefName {
	refname: String,
	format: NameFormat,
}

impl RefName {
	pub fn new(refname: &str, format: NameFormat) -> Result<Self> {
		Self::validate_name(refname, format)?;

		Ok(Self {
			refname: refname.to_owned(),
			format,
		})
	}
}

impl RefName {
	pub fn format(&self) -> NameFormat {
		self.format
	}

	// TODO: https://git-scm.com/docs/git-check-ref-format
	pub fn validate_name(_refname: &str, _format: NameFormat) -> Result<()> {
		Ok(())
	}
}

impl Display for RefName {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{}", self.refname)
	}
}

// hi woke friend, whats going on?
pub fn master() -> RefName {
	RefName {
		refname: "refs/heads/master".to_owned(),
		format: NameFormat::Normal,
	}
}

pub fn head() -> RefName {
	RefName {
		refname: "HEAD".to_owned(),
		format: NameFormat::OneLevel,
	}
}

pub fn fetch_head() -> RefName {
	RefName {
		refname: "FETCH_HEAD".to_owned(),
		format: NameFormat::OneLevel,
	}
}

pub fn orig_head() -> RefName {
	RefName {
		refname: "ORIG_HEAD".to_owned(),
		format: NameFormat::OneLevel,
	}
}

pub fn merge_head() -> RefName {
	RefName {
		refname: "MERGE_HEAD".to_owned(),
		format: NameFormat::OneLevel,
	}
}

pub fn cherry_pick_head() -> RefName {
	RefName {
		refname: "CHERRY_PICK_HEAD".to_owned(),
		format: NameFormat::OneLevel,
	}
}
