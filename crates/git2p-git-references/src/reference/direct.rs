/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{reference::refname::RefName, result::Result};
use git2p_git_oid::oid::Oid;
use serde::{Deserialize, Serialize};
use std::{
	fs::OpenOptions,
	io::Write,
	path::{Path, PathBuf},
};

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct DirectRef {
	refname: RefName,
	oid: Oid,
	git_dir: PathBuf,
}

impl DirectRef {
	pub fn new<P: AsRef<Path>>(refname: RefName, oid: &Oid, git_dir: P) -> Result<Self> {
		Ok(Self {
			refname,
			oid: *oid,
			git_dir: git_dir.as_ref().to_owned(),
		})
	}
}

impl DirectRef {
	pub fn update_target(self, target: Oid) -> Result<Self> {
		let mut file = OpenOptions::new()
			.create(false)
			.write(true)
			.read(true)
			.truncate(true)
			.open(self.path())?;

		file.write_all(target.to_string().as_bytes())?;

		Ok(self)
	}

	pub fn refname(&self) -> RefName {
		self.refname.to_owned()
	}

	pub fn target(&self) -> Oid {
		self.oid
	}

	pub(crate) fn git_dir(&self) -> &PathBuf {
		&self.git_dir
	}

	pub(crate) fn path(&self) -> PathBuf {
		self.git_dir.join(self.refname.to_string())
	}
}
