/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#![deny(
	deprecated,
	deprecated_in_future,
	useless_deprecated,
	clippy::clone_on_ref_ptr,
	clippy::clone_on_copy
)]
#![forbid(unsafe_code)]

use crate::reference::Reference;
use std::{
	fs, io,
	path::{Path, PathBuf},
};

pub mod error;
pub mod reference;
pub mod result;

pub(crate) struct PathInfo<'a, P: AsRef<Path>> {
	pub(crate) git_dir: &'a P,
	pub(crate) path: PathBuf,
}

pub fn all<'a, P: AsRef<Path>>(git_dir: P) -> result::Result<Vec<reference::Reference>> {
	find_at(git_dir, "refs")
}

pub fn heads<'a, P: AsRef<Path>>(git_dir: P) -> result::Result<Vec<reference::Reference>> {
	find_at(git_dir, "refs/heads")
}

pub fn tags<'a, P: AsRef<Path>>(git_dir: P) -> result::Result<Vec<reference::Reference>> {
	find_at(git_dir, "refs/tags")
}

pub fn remotes<'a, P: AsRef<Path>>(git_dir: P) -> result::Result<Vec<reference::Reference>> {
	find_at(git_dir, "refs/remotes")
}

fn find_at<'a, P: AsRef<Path>>(git_dir: P, at: &str) -> result::Result<Vec<reference::Reference>> {
	let mut files = vec![];

	for entry in fs::read_dir(git_dir.as_ref())? {
		let entry = entry?;
		let path = entry.path();
		if path.is_file() {
			files.push(path);
		}
	}

	let refs_dir = git_dir.as_ref().join(at);

	visit_dirs(&refs_dir, &mut files)?;

	Ok(files
		.into_iter()
		.filter_map(|p| {
			Reference::try_from(PathInfo {
				git_dir: &git_dir,
				path: p,
			})
			.ok()
		})
		.collect())
}

fn visit_dirs<P: AsRef<Path>>(dir: P, files: &mut Vec<PathBuf>) -> io::Result<()> {
	let ignore = |pattern: &str| -> bool { dir.as_ref().file_name().unwrap().eq(pattern) };

	if dir.as_ref().is_dir()
		&& !ignore("")
		&& !ignore("target")
		&& !ignore("scss")
		&& !ignore("pkg")
	{
		for entry in fs::read_dir(dir)? {
			let entry = entry?;
			let path = entry.path();
			if path.is_dir() {
				visit_dirs(&path, files)?;
			} else {
				if !path.is_symlink() {
					files.push(path);
				}
			}
		}
	}
	Ok(())
}
