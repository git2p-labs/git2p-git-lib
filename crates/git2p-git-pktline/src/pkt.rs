/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use std::fmt::Display;

pub trait Data: Into<Vec<u8>> {
	fn into_payload(self) -> Vec<u8> {
		self.into()
	}
}

impl<T: Into<Vec<u8>>> Data for T {}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Pkt {
	Data(Vec<u8>),
	Flush,
	Delimiter,
	ResponseEnd,
}

impl PartialEq<[u8]> for Pkt {
	fn eq(&self, other: &[u8]) -> bool {
		match self {
			Pkt::Data(_) => {
				other != [b'0', b'0', b'0', b'0']
					&& other != [b'0', b'0', b'0', b'1']
					&& other != [b'0', b'0', b'0', b'2']
			}
			Pkt::Flush => other == [b'0', b'0', b'0', b'0'],
			Pkt::Delimiter => other == [b'0', b'0', b'0', b'1'],
			Pkt::ResponseEnd => other == [b'0', b'0', b'0', b'2'],
		}
	}
}

impl Display for Pkt {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			Pkt::Data(d) => {
				let s = String::from_utf8_lossy(&d[..]);
				write!(f, "{s}")
			}
			Pkt::Flush => write!(f, "0000"),
			Pkt::Delimiter => write!(f, "0001"),
			Pkt::ResponseEnd => write!(f, "0002"),
		}
	}
}

impl Pkt {
	/// the payload shouldn't containt pktline size, it will calculated
	///
	/// # Panics
	///
	/// this method shouldn't entry in panic. If payload is empty then [`new_data`](Pkt::new_data)
	/// method will return a [Data Pkt](Pkt::Data) with 5 len and breakline as content.
	pub fn new_data<T: Data>(payload: T) -> Self {
		let mut payload = payload.into_payload();

		if payload.is_empty() {
			return Self::Data(vec![b'0', b'0', b'0', b'5', b'\n']);
		}

		if *payload.last().unwrap() != b'\n' {
			payload.push(b'\n');
		};

		let len = format!("{:04x}", payload.len() + 4);
		let len = len.as_bytes();

		let payload = [len, &payload].concat();

		Self::Data(payload)
	}

	#[must_use]
	pub fn inner_data(&self) -> Option<Vec<u8>> {
		if !&self.is_data_pkt() {
			return None;
		}

		let mut inner: Vec<_> = self.clone().into();
		let inner = inner.drain(4..);

		Some(inner.as_slice().to_owned())
	}

	#[must_use]
	pub fn is_data_pkt(&self) -> bool {
		matches!(self, Pkt::Data(_))
	}

	#[must_use]
	pub fn is_flush(&self) -> bool {
		matches!(self, Pkt::Flush)
	}

	#[must_use]
	pub fn is_delimiter(&self) -> bool {
		matches!(self, Pkt::Delimiter)
	}

	#[must_use]
	pub fn is_end(&self) -> bool {
		matches!(self, Pkt::ResponseEnd)
	}
}

impl<'a> From<&'a [u8]> for Pkt {
	fn from(value: &'a [u8]) -> Self {
		if value == [b'0', b'0', b'0', b'0'] {
			return Self::Flush;
		}

		if value == [b'0', b'0', b'0', b'1'] {
			return Self::Delimiter;
		}

		if value == [b'0', b'0', b'0', b'2'] {
			return Self::ResponseEnd;
		}

		Self::Data(value.to_vec())
	}
}

impl From<Pkt> for Vec<u8> {
	fn from(pkt: Pkt) -> Self {
		match pkt {
			Pkt::Data(pkt) => pkt,
			Pkt::Flush => vec![48u8; 4],
			Pkt::Delimiter => vec![b'0', b'0', b'0', b'1'],
			Pkt::ResponseEnd => vec![b'0', b'0', b'0', b'2'],
		}
	}
}
