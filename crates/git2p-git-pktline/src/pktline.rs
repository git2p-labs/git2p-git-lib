/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::pkt::{Data, Pkt};
use std::{
	fmt::Display,
	slice::{Iter, IterMut},
};

#[derive(Debug, Default)]
pub struct PktLine {
	pub(crate) pktline: Vec<Pkt>,
}

impl Display for PktLine {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		for pkt in &self.pktline {
			f.write_str(&pkt.to_string())?;
		}
		Ok(())
	}
}

impl PktLine {
	#[must_use]
	pub fn builder() -> Builder {
		Builder::new()
	}

	pub fn iter(&self) -> Iter<'_, Pkt> {
		self.pktline.iter()
	}

	pub fn iter_mut(&mut self) -> IterMut<'_, Pkt> {
		self.pktline.iter_mut()
	}

	pub fn remove(&mut self, index: usize) -> Pkt {
		self.pktline.remove(index)
	}
}

impl From<PktLine> for Vec<u8> {
	fn from(pktline: PktLine) -> Self {
		let mut vec = Vec::new();

		for pkt in pktline.pktline {
			vec.append(&mut pkt.into());
		}

		vec
	}
}

#[derive(Debug, Default)]
pub struct Builder {
	pktline: PktLine,
}

impl Builder {
	#[must_use]
	pub fn new() -> Self {
		Self::default()
	}

	pub fn add_flush(&mut self) -> &mut Self {
		self.pktline.pktline.push(Pkt::Flush);
		self
	}

	pub fn add_delimiter(&mut self) -> &mut Self {
		self.pktline.pktline.push(Pkt::Delimiter);
		self
	}

	pub fn add_response_end(&mut self) -> &mut Self {
		self.pktline.pktline.push(Pkt::ResponseEnd);
		self
	}

	pub fn add_data<T: Data>(&mut self, data: T) -> &mut Self {
		let pkt_data = Pkt::new_data(data);

		self.pktline.pktline.push(pkt_data);

		self
	}

	#[must_use]
	pub fn build(self) -> PktLine {
		self.pktline
	}
}
