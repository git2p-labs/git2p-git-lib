/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{error::Error, pkt::Pkt, pktline::PktLine, result::Result};

#[derive(Default)]
pub struct Reader {
	consumed: usize,
}

impl Reader {
	#[must_use]
	pub fn new() -> Self {
		Self::default()
	}

	/// # Errors
	///
	/// fails when pktline contains a malformed line
	pub fn read_from_bytes(&mut self, source: &[u8], end: &Pkt) -> Result<PktLine> {
		self.consumed = 0;

		let mut pktline = Vec::<Pkt>::new();

		let mut padding = 0;

		loop {
			let size = &source[padding..padding + 4];

			if end == size {
				padding += 4;
				pktline.push(Pkt::from(size));
				break;
			}

			if &Pkt::Flush == size || &Pkt::Delimiter == size || &Pkt::ResponseEnd == size {
				padding += 4;
				pktline.push(Pkt::from(size));
				continue;
			}

			let Ok(size) = usize::from_str_radix(&String::from_utf8_lossy(size), 16) else {
				return Err(Error::BadLineSize);
			};

			let data = &source[padding..padding + size];
			padding += size;

			pktline.push(Pkt::Data(data.to_vec()));
		}

		self.consumed = padding;

		Ok(PktLine { pktline })
	}

	#[must_use]
	pub fn consumed(&self) -> usize {
		self.consumed
	}
}
