/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use std::io::{BufRead, Cursor, Read};

use crate::{error::Error, result::Result};
use git2p_git_oid::oid::Oid;
use serde::{Deserialize, Serialize as Ser};
// use git2p_git_serializers::{de::TryDeserialize, ser::Serialize};

///
/// <`https://git-scm.com/docs/index-format#_index_entry`>
///
/// +---------------------------------------------------------------------------------+
/// |                                                                                 |
/// |                     INDEX ENTRY (DISK REPRESENTATION)                           |
/// |                                                                                 |
/// | +-----------+-----------+-----------+-----------+-----------+                   |
/// | |           |           |           |           |           |                   |
/// | | `ctime_s` | `ctime_n` | `mtime_s` | `mtime_n` |   `dev`   |                   |
/// | |           |           |           |           |           |                   |
/// | +-----------+-----------+-----------+-----------+-----------+                   |
/// |                                                                                 |
/// |      +-----------+----------+-----------+-----------+-----------+               |
/// |      |           |          |           |           |           |               |
/// |      |   `ino`   |  `mode`  |   `uid`   |   `gid`   |  `fsize`  |               |
/// |      |           |          |           |           |           |               |
/// |      +-----------+----------+-----------+-----------+-----------+               |
/// |                                                                                 |
/// |           +------------------------------------------------+                    |
/// |           |                                                |                    |
/// |           |        object name (hash in the git db)        |                    |
/// |           |                                                |                    |
/// |           +------------------------------------------------+                    |
/// |                                                                                 |
/// |                +---------------------------------------+                        |
/// |                |                flags                  |                        |
/// |                +---------------------------------------+                        |
/// |                |                                       |                        |
/// |                |    * * * * * * * * * * * * * * * *    |                        |
/// |                |    ^ ^ |_| |_____________________|    |                        |
/// |                |    | |  ^             ^               |                        |
/// |                +----+-+--+-------------+---------------+                        |
/// |                     | |  |             |                                        |
/// |                     | |  |             +-> name lenght                          |
/// |                     | |  |                                                      |
/// |                     | |  +-> stage                                              |
/// |                     | |                                                         |
/// |                     | +-> ext flags                                             |
/// |                     |                                                           |
/// |                     +-> assume valid                                            |
/// |                                                                                 |
/// |                     +---------------------------------------+                   |
/// |                     |           extended flags              |                   |
/// |                     +---------------------------------------+                   |
/// |                     |                                       |                   |
/// |                     |    * * * 0 0 0 0 0 0 0 0 0 0 0 0 0    |                   |
/// |                     |    ^ ^ ^ |_______________________|    |                   |
/// |                     |    | | |             ^                |                   |
/// |                     +----+-+-+-------------+----------------+                   |
/// |                          | | |             |                                    |
/// |                          | | |             +-> unused bits                      |
/// |                          | | |                                                  |
/// |                          | | +-> intent to add                                  |
/// |                          | |                                                    |
/// |                          | +-> skip worktree                                    |
/// |                          |                                                      |
/// |                          +-> reserved                                           |
/// |                                                                                 |
/// |                          +------------------------------------------------+     |
/// |                          |                                                |     |
/// |                          |         pathname (padded -> len % 8 == 0)      |     |
/// |                          |                                                |     |
/// |                          +------------------------------------------------+     |
/// |                                                                                 |
/// +---------------------------------------------------------------------------------+
///

pub(crate) const MAX_PATH_SIZE: u16 = 0xFFF;
// pub(crate) const BLOCK_PADDING_FACTOR: u8 = 8;

#[derive(Debug, Clone, PartialEq, Eq, Ser, Deserialize)]
pub struct Mode {
	object_kind: u8,
	unix_permissions: u16,
}

impl From<u32> for Mode {
	fn from(mode: u32) -> Self {
		let kind_mask = 0b1111_0000_0000_0000;

		let object_kind = u8::try_from((mode & kind_mask) >> 12).unwrap();

		let permissions_mask = 0b0000_0001_1111_1111;
		let unix_permissions = u16::try_from(mode & permissions_mask).unwrap();

		Self {
			object_kind,
			unix_permissions,
		}
	}
}

#[allow(clippy::struct_excessive_bools)]
#[derive(Debug, Clone, PartialEq, Eq, Ser, Deserialize)]
pub struct Entry {
	ctime: i32,
	ctime_nanos: i32,
	mtime: i32,
	mtime_nanos: i32,
	dev: u32,
	ino: u32,
	mode: Mode,
	uid: u32,
	gid: u32,
	file_size: u32,
	name: Oid,
	assume_valid: bool,
	extended_flags: bool,
	stage: u8,
	pathname_len: u16,
	intent_to_add: bool,
	skip_worktree: bool,
	path: String,
}

impl Ord for Entry {
	fn cmp(&self, other: &Self) -> std::cmp::Ordering {
		match self.path.cmp(&other.path) {
			std::cmp::Ordering::Equal => self.stage.cmp(&other.stage),
			ord => ord,
		}
	}
}

#[allow(clippy::non_canonical_partial_ord_impl)]
impl PartialOrd for Entry {
	fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
		match self.path.partial_cmp(&other.path) {
			Some(core::cmp::Ordering::Equal) => self.stage.partial_cmp(&other.stage),
			ord => ord,
		}
	}
}

impl From<&Entry> for Vec<u8> {
	fn from(_entry: &Entry) -> Self {
		todo!()
	}
}

impl From<Entry> for Vec<u8> {
	fn from(entry: Entry) -> Self {
		Self::from(&entry)
	}
}

impl TryFrom<&mut Cursor<&&[u8]>> for Entry {
	type Error = Error;

	fn try_from(cursor: &mut Cursor<&&[u8]>) -> Result<Self> {
		let mut ctime = [0u8; 4];
		let mut ctime_n = [0u8; 4];
		let mut mtime = [0u8; 4];
		let mut mtime_n = [0u8; 4];
		let mut dev = [0u8; 4];
		let mut ino = [0u8; 4];
		let mut mode = [0u8; 4];
		let mut uid = [0u8; 4];
		let mut gid = [0u8; 4];
		let mut fsize = [0u8; 4];

		cursor.read_exact(&mut ctime)?;
		cursor.read_exact(&mut ctime_n)?;
		cursor.read_exact(&mut mtime)?;
		cursor.read_exact(&mut mtime_n)?;
		cursor.read_exact(&mut dev)?;
		cursor.read_exact(&mut ino)?;
		cursor.read_exact(&mut mode)?;
		cursor.read_exact(&mut uid)?;
		cursor.read_exact(&mut gid)?;
		cursor.read_exact(&mut fsize)?;

		let ctime = i32::from_be_bytes(ctime);
		let ctime_nanos = i32::from_be_bytes(ctime_n);
		let mtime = i32::from_be_bytes(mtime);
		let mtime_nanos = i32::from_be_bytes(mtime_n);
		let dev = u32::from_be_bytes(dev);
		let ino = u32::from_be_bytes(ino);
		let mode = u32::from_be_bytes(mode);
		let uid = u32::from_be_bytes(uid);
		let gid = u32::from_be_bytes(gid);
		let file_size = u32::from_be_bytes(fsize);

		let mut name = [0u8; 20];
		cursor.read_exact(&mut name)?;

		let mut flags = [0u8; 2];

		cursor.read_exact(&mut flags)?;

		let flags = u16::from_be_bytes(flags);

		let assume_valid_mask = 0b1000_0000_0000_0000;
		let assume_valid = (flags & assume_valid_mask) != 0;

		let ext_flags_mask = 0b0100_0000_0000_0000;
		let extended_flags = (flags & ext_flags_mask) != 0;

		let stage_mask = 0b0011_0000_0000_0000;
		let stage = ((flags & stage_mask) >> 12) as u8;

		let pathlen_mask = 0b0000_1111_1111_1111;

		let pathname_len = (flags & pathlen_mask).min(MAX_PATH_SIZE);

		let (intent_to_add, skip_worktree) = if extended_flags {
			let mut ext_flags = [0u8; 2];

			cursor.read_exact(&mut ext_flags)?;

			let ext_flags = u16::from_be_bytes(ext_flags);

			let intent_to_add_mask = 0b0010_0000_0000_0000;
			let intent_to_add = (ext_flags & intent_to_add_mask) != 0;

			let skip_worktree_mask = 0b0100_0000_0000_0000;
			let skip_worktree = (ext_flags & skip_worktree_mask) != 0;

			(intent_to_add, skip_worktree)
		} else {
			(false, false)
		};

		let mut path = vec![];

		cursor.read_until(b'\0', &mut path)?;

		loop {
			let mut buf = [0u8; 1];

			cursor.read_exact(&mut buf)?;

			if buf[0] != b'\0' {
				break;
			}
		}

		let path = String::from_utf8_lossy(&path[..path.len() - 1]).to_string();

		Ok(Entry {
			ctime,
			ctime_nanos,
			mtime,
			mtime_nanos,
			dev,
			ino,
			mode: Mode::from(mode),
			uid,
			gid,
			file_size,
			name: Oid::try_from(&name[..])?,
			assume_valid,
			extended_flags,
			stage,
			pathname_len,
			intent_to_add,
			skip_worktree,
			path,
		})
	}
}
