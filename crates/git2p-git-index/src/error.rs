/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use std::{error, fmt::Display};

#[derive(Debug)]
pub enum Error {
	BadSignature,

	BadVersion,

	BadPath(git2p_git_path::error::Error),

	CorruptedIndex,

	IndexEntryIo(std::io::Error),

	Oid(git2p_git_oid::error::Error),
}

impl From<git2p_git_path::error::Error> for Error {
	fn from(value: git2p_git_path::error::Error) -> Self {
		Self::BadPath(value)
	}
}

impl From<std::io::Error> for Error {
	fn from(value: std::io::Error) -> Self {
		Self::IndexEntryIo(value)
	}
}

impl From<git2p_git_oid::error::Error> for Error {
	fn from(value: git2p_git_oid::error::Error) -> Self {
		Self::Oid(value)
	}
}

impl Display for Error {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{self:?}")
	}
}

impl error::Error for Error {}
