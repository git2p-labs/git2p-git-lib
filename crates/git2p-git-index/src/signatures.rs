/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

pub const INDEX_SIGNATURE: u32 = 0x44_49_52_43; // DIRC
pub const CACHE_TREE_SIGNATURE: u32 = 0x54_52_45_45; // TREE
pub const END_OF_INDEX_ENTRY_SIGNATURE: u32 = 0x45_4F_49_45; // EOIE
pub const FS_MONITOR_CACHE_SIGNATURE: u32 = 0x46_53_4D_4E; // FSMN
pub const INDEX_ENTRY_OFFSET_TABLE_SIGNATURE: u32 = 0x49_45_4F_54; // IEOT
pub const RESOLVE_UNDO_SIGNATURE: u32 = 0x52_45_55_43; // REUC
pub const SPARSE_DIRECTORY_ENTRIES_SIGNATURE: u32 = 0x73_64_69_72; // sdir
pub const SPLIT_INDEX_SIGNATURE: u32 = 0x6C_69_6E_6B; // link
pub const UNTRACKED_CACHE_SIGNATURE: u32 = 0x55_4E_54_52; // UNTR
