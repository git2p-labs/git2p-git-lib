/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{entry::Entry, error::Error, extensions, result::Result};
use git2p_git_oid::oid::Oid;
use git2p_git_serializers::{de::TryDeserialize, ser::Serialize};
use serde::{Deserialize, Serialize as Ser};
use sha1::{Digest, Sha1};
use std::io::{Cursor, Read, Seek, SeekFrom};

#[repr(u8)]
#[derive(Debug, Clone, PartialEq, Eq, Ser, Deserialize)]
pub enum Version {
	V2 = 2,
	V3 = 3,
	V4 = 4,
}

impl TryFrom<u8> for Version {
	type Error = Error;

	fn try_from(version: u8) -> Result<Self> {
		match version {
			2 => Ok(Version::V2),
			3 => Ok(Version::V3),
			4 => Ok(Version::V4),
			_ => Err(Error::BadVersion),
		}
	}
}

#[derive(Debug, Clone, PartialEq, Eq, Ser, Deserialize)]
pub struct Index {
	version: Version,
	len: u32,
	entries: Vec<Entry>,
	cache_tree: Option<extensions::cache_tree::CacheTree>,
	resolve_undo: Option<extensions::resolve_undo::ResolveUndo>,
	split_index: Option<extensions::split_index::SplitIndex>,
	untracked_cache: Option<extensions::untracked_cache::UntrackedCache>,
	fs_monitor_cache: Option<extensions::fs_monitor_cache::FsMonitorCache>,
	end_of_index_entry: Option<extensions::end_of_index_entry::EndIndexEntry>,
	index_entry_offset_table: Option<extensions::index_entry_offset_table::IndexEntryOffsetTable>,
	sparse_directory_entries: Option<extensions::sparse_directory_entries::SparseDirectoryEntries>,
}

git2p_git_serializers::serialize_impl!(Index, Vec<u8>, 'ser);

impl From<&Index> for Vec<u8> {
	fn from(_: &Index) -> Self {
		todo!()
	}
}

impl From<Index> for Vec<u8> {
	fn from(_: Index) -> Self {
		todo!()
	}
}

git2p_git_serializers::deserialize_impl!(Index, &'de [u8], Error, 'de);

impl TryFrom<&&[u8]> for Index {
	type Error = Error;

	fn try_from(raw: &&[u8]) -> Result<Self> {
		let mut cursor = Cursor::new(raw);

		let mut signature = [0u8; 4];
		cursor.read_exact(&mut signature)?;
		let signature = u32::from_be_bytes(signature);

		if signature.ne(&crate::signatures::INDEX_SIGNATURE) {
			return Err(Error::BadSignature);
		}

		let mut expected_hash = [0u8; 20];
		cursor.seek(SeekFrom::End(-20))?;
		cursor.read_exact(&mut expected_hash)?;

		let expected_hash = Oid::try_from(expected_hash.as_slice())?;

		let mut hasher = Sha1::new();
		hasher.update(&raw[..raw.len() - 20]);
		let hash = Oid::try_from(hasher.finalize().as_slice())?;

		if hash.ne(&expected_hash) {
			return Err(Error::CorruptedIndex);
		}

		cursor.seek(SeekFrom::Start(4))?;
		let mut version = [0u8; 4];

		cursor.read_exact(&mut version).unwrap();

		let version = u32::from_be_bytes(version);

		match Version::try_from(u8::try_from(version).unwrap())? {
			Version::V2 => deserialize_v2(&mut cursor),
			Version::V3 => todo!(),
			Version::V4 => todo!(),
		}
	}
}

fn deserialize_v2(cursor: &mut Cursor<&&[u8]>) -> Result<Index> {
	cursor.seek(SeekFrom::Start(8))?;

	let mut len = [0u8; 4];

	cursor.read_exact(&mut len)?;

	let len = u32::from_be_bytes(len);

	let entries = (0..len)
		.map(|_| {
			let entry = Entry::try_from(&mut *cursor).unwrap();

			cursor.seek_relative(-1).unwrap();

			entry
		})
		.collect();

	let mut cache_tree = None;

	loop {
		let mut next_signature = [0u8; 4];

		cursor.read_exact(&mut next_signature)?;

		match u32::from_be_bytes(next_signature) {
			crate::signatures::CACHE_TREE_SIGNATURE => {
				cache_tree = Some(extensions::cache_tree::CacheTree::try_from(&mut *cursor)?);
			}
			crate::signatures::END_OF_INDEX_ENTRY_SIGNATURE => todo!(),
			crate::signatures::FS_MONITOR_CACHE_SIGNATURE => todo!(),
			crate::signatures::INDEX_ENTRY_OFFSET_TABLE_SIGNATURE => todo!(),
			crate::signatures::RESOLVE_UNDO_SIGNATURE => todo!(),
			crate::signatures::SPARSE_DIRECTORY_ENTRIES_SIGNATURE => todo!(),
			crate::signatures::SPLIT_INDEX_SIGNATURE => todo!(),
			crate::signatures::UNTRACKED_CACHE_SIGNATURE => todo!(),
			_ => {}
		}

		if usize::try_from(cursor.position()).unwrap() >= cursor.get_ref().len() - 20 {
			break;
		}
	}

	Ok(Index {
		version: Version::V2,
		len,
		entries,
		cache_tree,
		resolve_undo: None,
		split_index: None,
		untracked_cache: None,
		fs_monitor_cache: None,
		end_of_index_entry: None,
		index_entry_offset_table: None,
		sparse_directory_entries: None,
	})
}

#[test]
fn deserialize() {
	let path = "/home/al3x/home_workspace/material_you_rs/.git/index";

	let mut file = std::fs::File::open(path).unwrap();

	let mut content = vec![];

	file.read_to_end(&mut content).unwrap();

	let index = Index::try_deserialize(&&content[..]).unwrap();

	dbg!(&index.version);
	dbg!(&index.len);
	dbg!(&index.entries.len());
	dbg!(&index.cache_tree);
}
