/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

//!
//! +-----------------------------------------------+
//! | +-------------+--------------+--------------+ |
//! | |             |              |              | |
//! | | `signature` |  `ext_size`  |  `ext_data`  | |
//! | |             |              |              | |
//! | +------/\-----+--------------+--------------+ |
//! |        ||                                     |
//! +--------||-------------------------------------+
//!          ||
//!          ||
//!        +-----+-----+-----+-----+
//!        |     |     |     |     |
//!        |  *  |  *  |  *  |  *  |
//!        |     |     |     |     |
//!        +-----+-----+-----+-----+
//!
//! `ext_size` tells the size of the serialized (on disk) ext's data
//!

pub mod cache_tree;
pub mod end_of_index_entry;
pub mod fs_monitor_cache;
pub mod index_entry_offset_table;
pub mod resolve_undo;
pub mod sparse_directory_entries;
pub mod split_index;
pub mod untracked_cache;

// pub enum Extensions {
// 	CacheTree,
// 	EndOfIndexEntry,
// 	FsMonitorCache,
// 	IndexEntryOffsetTable,
// 	ResolveUndo,
// 	SparseDirectoryEntries,
// 	SplitIndex,
// 	UntrackedCache,
// }
