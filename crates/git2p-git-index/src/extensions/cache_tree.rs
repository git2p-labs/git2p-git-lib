/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{error::Error, result::Result};
use git2p_git_oid::oid::Oid;
use serde::{Deserialize, Serialize as Ser};
use std::io::{BufRead, Cursor, Read};

///
/// +----------------------------------------------------------+
/// |              CACHE TREE (DISK REPRESENTATION)            |
/// |                                                          |
/// |        +---------------------------+------------+        |
/// |        | +-----+-----+-----+-----+ |            |        |
/// |        | |     |     |     |     | |            |        |
/// |        | | `T` | `R` | `E` | `E` | | `ext_size` |        |
/// |        | |     |     |     |     | |            |        |
/// |        | +-----+-----+-----+-----+ |            |        |
/// |        +------------/\-------------+------------+        |
/// |                     ||                                   |
/// |               ext's signature                            |
/// |                                                          |
/// |            +----------------------------------+          |
/// |            |            ext's data            |          |
/// |            | +------------------------------+ |          |
/// |            | |      `cache_tree_entry`      | |          |
/// |            | +------------------------------+ |          |
/// |            | |      `cache_tree_entry`      | |          |
/// |            | +------------------------------+ |          |
/// |            | |      `cache_tree_entry`      | |          |
/// |            | +------------------------------+ |          |
/// |            | |             ...              | |          |
/// |            | +------------------------------+ |          |
/// |            | |             ...              | |          |
/// |            | +------------------------------+ |          |
/// |            | |             ...              | |          |
/// |            | +------------------------------+ |          |
/// |            | |      `cache_tree_entry`      | |          |
/// |            | +------------------------------+ |          |
/// |            | |      `cache_tree_entry`      | |          |
/// |            | +------------------------------+ |          |
/// |            | |      `cache_tree_entry`      | |          |
/// |            | +------------------------------+ |          |
/// |            +----------------------------------+          |
/// +----------------------------------------------------------+
///
#[derive(Debug, Clone, PartialEq, Eq, Ser, Deserialize)]
pub struct CacheTree {
	entries: Vec<CacheTreeEntry>,
}

impl TryFrom<&mut Cursor<&&[u8]>> for CacheTree {
	type Error = Error;

	fn try_from(cursor: &mut Cursor<&&[u8]>) -> Result<Self> {
		let start = cursor.position();

		let mut size = [0u8; 4];
		cursor.read_exact(&mut size)?;

		let size = u32::from_be_bytes(size);

		let end = start + u64::from(size) + 4;

		let mut entries = vec![];

		loop {
			let entry = CacheTreeEntry::try_from(&mut *cursor)?;
			dbg!(&entry);
			entries.push(entry);

			dbg!(&cursor.get_ref().len());
			dbg!(&start);
			dbg!(&cursor.position());
			dbg!(&end);
			if cursor.position() >= end {
				dbg!("break");
				break;
			}
		}

		Ok(Self { entries })
	}
}

///
/// +------------------------------------------------------------------------------------+
/// |                 CACHE TREE ENTRY (DISK REPRESENTATION)                             |
/// | +--------+--------+---------------+--------+--------------+--------+-------------+ |
/// | |        |        |               |        |              |        |             | |
/// | | `path` |  `\0`  | `entry_count` |  `SP`  | `tree_count` |  `LF`  | `tree_name` | |
/// | |        |        |               |        |              |        |    (oid)    | |
/// | +--------+--------+---------------+--------+--------------+--------+-------------+ |
/// +------------------------------------------------------------------------------------+
///
#[allow(clippy::module_name_repetitions)]
#[derive(Debug, Clone, PartialEq, Eq, Ser, Deserialize)]
pub struct CacheTreeEntry {
	path: String,
	entry_count: u32,
	tree_count: u32,
	name: Oid,
}

impl TryFrom<&mut Cursor<&&[u8]>> for CacheTreeEntry {
	type Error = Error;

	fn try_from(cursor: &mut Cursor<&&[u8]>) -> Result<Self> {
		let mut path = vec![];

		cursor.read_until(b'\0', &mut path)?;

		let path = String::from_utf8(path[..path.len() - 1].to_vec()).unwrap();

		let mut entry_count = vec![];
		cursor.read_until(b' ', &mut entry_count).unwrap();
		let entry_count = String::from_utf8(entry_count[..entry_count.len() - 1].to_vec()).unwrap();
		let entry_count = entry_count.parse().unwrap();

		let mut tree_count = vec![];
		cursor.read_until(b'\n', &mut tree_count).unwrap();
		let tree_count = String::from_utf8(tree_count[..tree_count.len() - 1].to_vec()).unwrap();
		let tree_count = tree_count.parse().unwrap();

		let mut name = [0u8; 20];

		cursor.read_exact(&mut name).unwrap();

		Ok(CacheTreeEntry {
			path,
			entry_count,
			tree_count,
			name: Oid::try_from(name.as_slice())?,
		})
	}
}
