/* git2p-git-lib - git functionality and primitives as library written in rust
 * Copyright (C) 2023 al3x
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

use crate::{error::Error, result::Result};
use git2p_git_helpers::{
	ascii::{GT, LT, SPACE},
	convert::{AsStr, SplitAll},
};
use git2p_git_serializers::{de::TryDeserialize, ser::Serialize};
use git2p_git_time::time::Time;
use serde::{Deserialize, Serialize as Ser};
use std::fmt::Display;

#[derive(Clone, Debug, Eq, PartialEq, Ser, Deserialize)]
pub struct Author {
	name: String,
	email: String,
	time: Time,
}

impl Author {
	pub fn new(name: &str, email: &str) -> Result<Self> {
		Ok(Self {
			name: name.to_owned(),
			email: email.to_owned(),
			time: Time::now_local().map_or(Time::now_utc()?, |t| t),
		})
	}

	pub fn name(&self) -> String {
		self.name.clone()
	}

	pub fn email(&self) -> String {
		self.email.clone()
	}

	pub fn time(&self) -> Time {
		self.time
	}
}

impl Display for Author {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(
			f,
			"{} <{}> {}",
			self.name,
			self.email,
			self.time.format_ts_tz()
		)
	}
}

git2p_git_serializers::serialize_impl!(Author, Vec<u8>, 'ser);

impl From<&Author> for Vec<u8> {
	fn from(a: &Author) -> Self {
		[
			a.name.as_bytes(),
			&[SPACE, LT],
			a.email.as_bytes(),
			&[GT, SPACE],
			&Serialize::serialize(&a.time),
		]
		.concat()
	}
}

git2p_git_serializers::deserialize_impl!(Author, &'de [u8], Error, 'de);

impl TryFrom<&&[u8]> for Author {
	type Error = Error;

	fn try_from(v: &&[u8]) -> Result<Self> {
		let parts = v.split_all(&SPACE)?;

		if parts.len().ne(&4) {
			return Err(Error::BadDeserializationInput);
		}

		let name = parts[0].as_str()?;

		let email = &parts[1][1..parts[1].len() - 1];
		let email = email.as_str()?;

		let time = [parts[2], &[SPACE], parts[3]].concat();

		let time = Time::try_deserialize(&time.as_slice())?;

		Ok(Self {
			name: name.to_owned(),
			email: email.to_owned(),
			time,
		})
	}
}
